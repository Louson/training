# Create a custom machine configuration

***Let Poky know about your hardware!***

During this lab, you will:

* Create a custom machine configuration
* Understand how the target architecture is dynamically chosen

## Create a custom machine

The machine file configures various hardware related settings. That’s what we
did in lab1, when we chose the beaglebone one. While it is not necessary to make
our custom machine image here, we’ll create a new one to demonstrate the
process.

Add a new `syslinbitlabs` machine to the previously created layer, which will make
the BeagleBone properly boot.

This machine describes a board using the `cortexa8thf-neon` tune and is a part
of the `ti33x` SoC family. Add the following lines to your machine configuration
file:

``` bash
require conf/machine/include/ti-soc.inc
SOC_FAMILY:append = ":ti33x"
DEFAULTTUNE = "armv7athf-neon"
require conf/machine/include/arm/armv7a/tune-cortexa8.in
```

## Populate the machine configuration

This `syslinbitlabs` machine needs:

* To select `linux-ti-staging` as the preferred provider for the kernel.
* To build ` am335x-boneblack.dtb` and the `am335x-boneblack-wireless.dtb`
device trees.
* To select `u-boot-ti-staging` as the preferred provider for the bootloader.
* To use arm as the U-Boot architecture.
* To use `am335x_evm_config` as the U-Boot configuration target.
* To use `0x80008000` as the U-Boot entry point and load address.
* To use a `zImage` kernel image type.
* To configure one serial console to `115200 ttyS0`
* And to support some features:
  - `apm`
  - `usbgadget`
  - `usbhost`
  - `vfat`
  - `ext2`
  - `alsa`

## Build an image with the new machine

You can now update the `MACHINE` variable value in the local configuration and
start a fresh build.

## Check generated files are here and correct

Once the generated images supporting the new `syslinbitlabs` machine are generated,
you can check all the needed images were generated correctly.

Have a look in the output directory, in
`$BUILDDIR/tmp/deploy/images/syslinbitlabs/`. Is there something missing?

## Update the rootfs

You can now update your root filesystem, to use the newly generated image
supporting our `syslinbitlabs` machine!

## Going further

We chose a quite generic tune (`armv7athf-neon`). It’s the same one as
`meta-ti`’s definition for the Beaglebone machine. You can see what Bitbake did
in `$BUILDDIR/tmp/work`.

Now, we can change the tune to `cortexa8thf-neon`. Rebuild the image, and look
at `$BUILDDIR/tmp/work`. What happened ?
