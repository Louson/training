# Create a Yocto layer

***Add a custom layer to the Yocto project for your project needs***

During this lab, you will:

* Create a new Yocto layer
* Interface this custom layer to the existing Yocto project
* Use applications from custom layers

This lab extends the previous one, in order to fully understand how to interface
a custom project to the basic Yocto project.

## Tools

You can access the configuration and state of layers with the bitbake-layers
command. This command can also be used to retrieve useful information about
available recipes. Try the following commands:

``` bash
bitbake-layers show-layers
bitbake-layers show-recipes linux-yocto
bitbake-layers show-overlayed
bitbake-layers create-layer
```

## Create a new layer

With the above commands, create a new Yocto layer named `meta-slb-labs` with a
priority of 7. Before doing that, return to your project root directory, where
by convention all layers are stored (`cd $HOME/yocto-labs/`).


Before using the new layer, we need to configure its generated configuration
files. You can start with the README file which is not used in the build process
but contains information related to layer maintenance. You can then check, and
adapt if needed, the global layer configuration file located in the `conf`
directory of your custom layer.

## Integrate a layer to the build

To be fair, we already used and integrated a layer in our build configuration
during the first lab, with `meta-ti`. This layer was responsible for BeagleBone
Black support in Yocto. We have to do the same for our `meta-slb-labs` now.

There is a file which contains all the paths of the layers we use. Try to find
it without looking back to the first lab. Then add the full path to our newly
created layer to the list of layers. Validate the integration of the
`meta-slb-labs` layer with:

``` bash
bitbake-layers show-layers
```

and make sure you don’t have any warning from bitbake.

## Add a recipe to the layer

In the previous lab we introduced a recipe for the nInvaders game. We included
it to the existing meta layer. While this approach give a working result, the
Yocto logic is not respected. You should instead always use a custom layer to
add recipes or to customize the existing ones. To illustrate this we will move
our previously created nInvaders recipe into the `meta-slb-labs` layer.

You can check the nInvaders recipe is part of the meta layer first:

``` bash
bitbake-layers show-recipes ninvaders
```

Then move the nInvaders recipe to the `meta-slb-labs` layer. You can check that
the `nInvaders` recipe is now part of the layer with the `bitbake-layers`
command.
