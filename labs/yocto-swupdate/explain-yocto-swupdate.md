# Update OTA explanations

## Build the `swu` image and update the rootfs

`sw-description`:
``` json
software =
{
    version = "1.0";

    hardware-compatibility: [ "1.0" ];

    images: (
        {
            filename = "lab-image-beaglebone.ext4.gz";
            device = "/dev/mmcblk0p2";
            compressed = "zlib";
        }
    );
}
```

`slb-labs-simple-swu_1.0.bb`
``` bash
SUMMARY = "Simple swupdate image"
DESCRIPTION = "Make a rootfs update"
LICENSE = "MIT"

inherit swupdate

SRC_URI += " \
	file://MIT \
	file://sw-description \
	"

LIC_FILES_CHKSUM = "file://MIT;md5=477dfa54ede28e2f361e7db05941d7a7"

IMAGE_DEPENDS = "slb-labs-image"

SWUPDATE_IMAGES = "slb-labs-image"
SWUPDATE_IMAGES_FSTYPES[slb-labs-image] = ".ext4.gz"
```

The command to run the update is:
`swupdate -H slb:1.0 -i slb-labs-simple-swu-beaglebone.swu`

## Dual copy with fall-back

### Setup

The `slb.wks`:
