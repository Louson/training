SUMMARY="Keys and certificates for slb-labs"
SECTION="examples"

LICENSE="MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

FILESEXTRAPATHS_prepend := "${HOME}/yocto-labs:"

SRC_URI="\
	file://aeskeys/public.pem \
	file://swupdate-pubkey.conf"

do_install() {
	install -d ${D}${sysconfdir}/slb-labs-certificates
	install -m 0444 ${WORKDIR}/aeskeys/public.pem ${D}${sysconfdir}/slb-labs-certificates/public.pem
	install -d ${D}${libdir}/swupdate/conf.d
	install -m 0644 ${WORKDIR}/swupdate-pubkey.conf ${D}${libdir}/swupdate/conf.d/40-pubkey
}

FILES_${PN} += "${libdir}/swupdate"
