# Update OTA with swupdate

Look for `meta-swupdate` in the index and check the dependencies. We miss one.

Add the needed layers to the build:

``` bash
git clone git://git.openembedded.org/meta-openembedded
bitbake-layers add-layer meta-openembedded/meta-oe
git clone https://github.com/sbabic/meta-swupdate.git
bitbake-layers add-layer meta-swupdate
```

## Setup

In the image, we need to:

1. add to the image the needed packages for SWUpdate
1. configure uboot environment
1. change the sdcard layout

### Add the needed packages

* `libconfig`
* `swupdate`
* `swupdate-www`

### Configure uboot environment

If you look in the uboot configuration, you will find that the environment is
located in a vfat filesystem in the first partition of the mmc. There is no
offset and the size is `0x20000`.

``` bash
grep CONFIG_ENV $BUILDDIR/tmp/work/beaglebone-*/u-boot-ti-staging/*/build/.config
```

First we need to create a mountpoint for this partition. Append the `base-files`
recipe to:

1. create `/boot/boot` directory add a line in `/etc/fstab`:
   ``` bash
   /dev/mmcblk0p1 /boot/boot auto defaults,sync,noauto  0  0
   ```
1. add the file `/etc/fw_env.config`:
   ``` bash
   /boot/boot/uboot.env 0x00000 0x20000
   ```

### Change the SDcard layout

Copy the `wks` file actually used (`meta-ti/wic/sdimage-2part.wks`) and add a
second rootfs partition (do not set any mountpoint). Change the image to take
that configuration build the sdcard image (`wic` or `wic.xz`). Plug the sdcard
and erase it:

``` bash
xz -dc $BUILDDIR/tmp/deploy/images/beaglebone/core-image-minimal-beaglebone.wic.xz \
  | sudo dd of=/dev/sdX conv=fdatasync bs=4M
```

Boot on the sdcard without setting the `NFS`. Check we have booted on the 2nd
partition.

## Build the `swu` image and update the rootfs

Check that `slb-labs-image` deploys a raw rootfs (`ext4`, `ext4.gz`...).

We now want to install that new image on the sdcard. Only the rootfs will be written.

Create a `swu` image recipe called `slb-labs-simple-swu` in `meta-syslinbitlab`
and the associated `sw-description` that will create an update package with a
rootfs that shall erase `/dev/mmcblk0p2`.

Once the image is built, copy it to the target and run the update with
`swupdate` using the option `-H <board>:<revision>`.

Create the file `/etc/hwrevision` and use the command `swupdate-client` to run
the update.

Finally, run the update through the web interface (`http://<ip>:8080`).

## Dual copy with fall-back

### Update the copy

Create a new `swu` image recipe called `slb-labs-swu` using the `sw-description`
file given in the labs data.

``` bash
mkdir $HOME/yocto-labs/meta-syslinbitlab/recipes-core/images/slb-labs-swu/
cp $HOME/yocto-labs/sw-description \
  $HOME/yocto-labs/meta-syslinbitlab/recipes-core/images/slb-labs-swu/
```

Use the web interface (`http://<ip>:8080`) to send the new built image. Check
the error in the messages at the bottom of the page.

```
ERROR : Compatible SW not found
```

#### Step 1 - configure the daemon

The daemon needs the software informations. Here is a script that dynamically
checks the partition to launch swupdate with the corresponding arguments:

`/usr/lib/swupdate/conf.d/09-swupdate-args`:
``` bash
rootfs=`mount | grep "on / type" | cut -d':' -f2 | cut -d' ' -f1`

if [ $rootfs == '/dev/mmcblk0p2' ];then
	selection="-e stable,faceA"
else
	selection="-e stable,faceB"
fi

SWUPDATE_ARGS="-v -H beaglebone:1.0 ${selection}"
```

Put that script in the update image and change the `sw-description` to install
that file on any software.

Append the recipe `swupdate` to include that script, so we have it in all future
images.

Rebuild the update image and run the update. Reboot the board and restart
swupdate. Check swupdate runs with the rights arguments.
``` bash
/etc/init.d/swupdate restart
ps | grep swupdate
```

### Step 2 - finalize the image and update

Run again the update. Mount `/dev/mmcblk0p3` to check the rootfs has been
installed.

Reset the board and stop the boot. Change the loaded partition to `mmc2`.

## Secure the update

### Block image downgrade

Add to `sw-description` the fields `name = "rootfs"` `version = "1.0"`[^autoversion] and
`install-if-higher=true`.

[^autoversion]: The variable @SWUPDATE_AUTO_VERSION does not work with images

Create the file `/etc/sw-versions`:
``` bash
rootfs		1.0
```

Rebuild, restart `swupdate` and try the update. Now the update will fail until
we increase the image's version.

We can see the choice of blocking downgrade is up to the package. Therefore it
is needed to first check image's origin to face cyberattacks.

### Check image's integrity

#### Update swupdate to accept signed images

Run swupdate's configuration: `bitbake swupdate -c menuconfig` (this is not
permanent) and set: `Enable verification of signed images`

We need to give the public AES key to swupdate. We can use the pre-generated
keys in the labs data. Use the given `slb-labs-certificate` recipe:

``` bash
mkdir $HOME/meta-slb-labs/recipes-support/
cp $HOME/yocto-labs/slb-labs-certificates $HOME/meta-slb-labs/recipes-support/
```

Include `openssl` and `slb-labs-certificates` and increase the version in
`sw-description`

Build and run the update.

Now we can only update with signed images.

#### Build a signed image

Add the checksum in `sw-description`:
``` bash
			sha256 = "@slb-labs-image-beaglebone.ext4.gz"
```

Define in `local.conf:

``` bash
SWUPDATE_SIGNING = RSA
SWUPDATE_PRIVATE_KEY = "${HOME}/yocto-labs/aeskeys/priv.pem"
SWUPDATE_PASSWORD_FILE = "${HOME}/yocto-labs/aeskeys/priv.pw"
```

## Extra: manage boot sequence


