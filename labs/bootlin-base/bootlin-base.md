
### Right to copy

Based on a [bootlin training](https://bootlin.com/doc/training/).

© Copyright 2004-2021, Bootlin  
**License: [Creative Commons Attribution - Share Alike
3.0](https://creativecommons.org/licenses/by-sa/3.0/legalcode)**

> You are free:
> 
> * to copy, distribute, display, and perform the work
> * to make derivative works
> * to make commercial use of the work
> 
> Under the following conditions:
> 
> * **Attribution**. You must give the original author credit.
> * **Share Alike**. If you alter, transform, or build upon this work, you may
>   distribute the resulting work only under a license identical to this one.
> * For any reuse or distribution, you must make clear to others the license terms of this work.
> * Any of these conditions can be waived if you get permission from the copyright holder.
> 
> Your fair use and other rights are in no way affected by the above.

**Document sources**: <https://github.com/bootlin/training-materials/>

### Hyperlinks in the document

There are many hyperlinks in the document

* Regular hyperlinks:  
<https://kernel.org/>
* Kernel documentation links:  
[dev-tools/kasan](https://www.kernel.org/doc/html/latest/dev-tools/kasan.html)
* Links to kernel source files and directories:
[drivers/input/](https://elixir.bootlin.com/linux/latest/source/drivers/input/)  
[include/linux/fb.h](https://elixir.bootlin.com/linux/latest/source/include/linux/fb.h)
* Links to the declarations, definitions and instances of kernel symbols (functions,
types, data, structures):  
[`platform_get_irq()`](https://elixir.bootlin.com/linux/latest/ident/platform_get_irq)  
[GFP_KERNEL](https://elixir.bootlin.com/linux/latest/ident/GFP_KERNEL)  
[struct
file_operations](https://elixir.bootlin.com/linux/latest/ident/file_operations)
