# Add a custom application

***Add a new recipe to support a required custom application***

During this lab, you will:

* Write a recipe for a custom application
* Integrate this application into the build

This is the first step of adding an application to Yocto. The remaining part is covered in the
next lab, *"Create a Yocto layer"*.

## Setup and organization

In this lab we will add a recipe handling the `nInvaders` application. Before
starting the recipe itself, find the `recipes-extended` directory in the
OpenEmbedded-Core layer and add a subdirectory for your application.

A recipe for an application is usually divided into a version specific `bb` file
and a common one.  Try to follow this logic and separate the configuration
variables accordingly.

**Tip**: it is possible to include a file into a recipe with the keyword `require`.

## First hands on nInvaders

The nInvaders application is a terminal based game following the space invaders family. In order
to deal with the text based user interface, nInvaders uses the `ncurses` library.

First try to find the project homepage, download the sources and have a first
look: license, Makefile, requirements...

## Write the common recipe

Create an appropriate common file, ending in `.inc`

In this file add the common configuration variables: source URI, description...

## Write the version specific recipe

Create a file that respects the Yocto nomenclature: `${PN}_${PV}.bb`

Add the required common configuration variables: archive checksum, license file checksum,
package revision...

## Testing and troubleshooting

You can check the whole packaging process is working fine by explicitly running the build task
on the nInvaders recipe:

``` bash
bitbake ninvaders
```

Try to make the recipe on your own. Also eliminate the warnings related to your recipe: some
configuration variables are not mandatory but it is a very good practice to define them all.

If you hang on a problem, check the following points:

* The common recipe is included in the version specific one
* The checksum and the URI are valid
* The dependencies are explicitly defined
* The internal state has changed, clean the working directory:
  ``` bash
  bitbake -c cleanall ninvaders
  ```

One of the build failures you will face will generate many messages such as:
```bash
multiple definition of `skill_level'; aliens.o:(.bss+0x674): first defined
here```

The `multiple definition` issue is due to the code base of `nInvaders` being
quite old, and having multiple compilation units redefine the same
symbols. While this was accepted by older `gcc` versions, since `gcc 10` this is
no longer accepted by default.

While we could fix the `nInvaders` code base, we will take a different route:
ask `gcc` to behave as it did before `gcc 10` and accept such
redefinitions. This can be done by passing the `-fcommon` `gcc` flag.

To achieve this, make sure to add `-fcommon` to the `CFLAGS` variable.

**Tip**: BitBake has command line flags to increase its verbosity and activate
debug outputs. Also, remember that you need to cross-compile `nInvaders` for
ARM! Maybe, you will have to configure your recipe to resolve some mistakes done
in the application’s Makefile (which is often the case). A bitbake variable
permits to add some Makefile’s options, you should look for it.

## Update the rootfs and test

Now that you’ve compiled the `nInvaders` application, generate a new rootfs
image with `bitbake core-image-minimal`. Then update the NFS root directory. You
can confirm the `nInvaders` program is present by running:

```bash
find /nfs -iname ninvaders
```

Access the board command line through SSH. You should be able to launch the nInvaders
program. Now, it’s time to play!

## Inspect the build

The `nInvaders` application was unpacked and compiled in the recipe’s work
directory. Can you spot nInvaders’ directory in the build work directory?

Once you found it, look around. You should at least spot some directories:

* The sources. Remember the `${S}` variable?
* `temp`. There are two kinds of files in there. Can you tell what are their
  purposes?
* Try to see if the licences of nInvaders were extracted.
