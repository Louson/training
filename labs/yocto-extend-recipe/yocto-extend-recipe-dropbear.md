# Extend a recipe

***Add your features to an existing recipe***

During this lab, you will:

* Apply patches to an existing recipe
* Use a custom configuration file for an existing recipe
* Extend a recipe to fit your needs

## Create a basic appended recipe

To avoid rewriting recipes when a modification is needed on an already existing
one, BitBake allows to extend recipes and to overwrite, append or prepend
configuration variables values through the so-called BitBake append files.

We will first create a basic BitBake append file, without any change made to the
original recipe, to see how it is integrated into the build. We will then extend
some configuration variables of the original recipe.

Try to create an appended recipe with the help of the online Yocto Project
development documentation. You can find it at
<https://docs.yoctoproject.org/dev-manual/index.html>. We here aim to extend the
`dropbear` recipe.

You can see available `bbappend` files and the recipe they apply to by using the
`bitbake-layers` tool (again!):

``` bash
bitbake-layers show-appends
```

If the BitBake append file you just created is recognized by your Yocto environment, you should
see:

``` bash
dropbear_2019.78.bb:
$HOME/yocto-labs/meta-slb-labs/recipes-core/dropbear/dropbear_%.bbappend
```

## Add patches to apply in the recipe

We want our extended `dropbear` to secure the ssh connection.

One easy thing to do is to change the SSH port (22 by default). Watch the init
script and find where the port is defined.

Now install `ssh-audit` and analyze our platform (don't forget to specify the
port if you have changed it). There we can see we are using some weak
algorithms. To fix that, we need to patch the file `default_options.h`.

Applying a patch is a common task in the daily Yocto process. Many recipes,
appended or not, apply a specific patch on top of a mainline project. It’s why
patches do not have to be explicitly applied, if the recipe inherits from the
patch class (directly or not), but only have to be present in the source files
list.

Try to create a patch with `quilt` and add it to your BitBake append file.

It is handled automatically in the `dropbear` original recipe.

You can now rebuild `dropbear` to take the new patches into account:

``` bash
bitbake dropbear
```

## Test the new recipe

Copy the newly generated dropbear into you `/nfs` and restart dropbear. Check
the situation with `ssh-audit`.
