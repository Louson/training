# First Yocto Project build

***Your first dive into Yocto Project and its build mechanism***

During this lab, you will:

* Set up an OpenEmbedded environment
* Configure the project and choose a target
* Build your first Poky image

## Setup

Before starting this lab, make sure your home directory is not encrypted using
`eCryptFS`. OpenEmbedded cannot be used on top of an `eCryptFS` file system due
to limitations in file name lengths.

Go to the `$HOME/yocto-labs/` directory.

Install the required packages:

``` bash
sudo apt install bc build-essential chrpath cpio \
    diffstat gawk git python texinfo wget
```

## Download Yocto

Download the kirkstone version of Poky:

``` bash
cd $HOME/yocto-labs
git clone https://git.yoctoproject.org/git/poky
cd poky
git checkout -b yocto-4.0.2 yocto-4.0.2
```

Return to your project root directory (`cd $HOME/yocto-labs/`) and download the
`meta-arm` and `meta-ti` layers:

``` bash
cd $HOME/yocto-labs
git clone https://git.yoctoproject.org/git/meta-arm
git checkout -b yocto-4.0 yocto-4.0
```

```bash
cd $HOME/yocto-labs
git clone https://git.yoctoproject.org/git/meta-ti
cd meta-ti
git checkout -b kirkstone-labs 2124e7ecd88d1aded320e86da62785c3ab171b27
git am $HOME/yocto-labs/0001-Simplify-linux-ti-staging-recipe-for-the-Bootlin-lab.patch
```

## Set up the build environment

Check you’re using Bash. This is the default shell when using Ubuntu.
Export all needed variables and set up the build directory:

``` bash
cd $HOME/yocto-labs
source poky/oe-init-build-env
```

You must specify which machine is your target. By default it is `qemu`. We need to build an
image for a beaglebone. Update the `MACHINE` configuration variable accordingly. Be careful,
beaglebone is different from the beagleboard machine!

Also, if you need to save disk space on your computer you can add `INHERIT += "rm_work"` in
the previous configuration file. This will remove the package work directory once a package is
built.

Don’t forget to make the configuration aware of the ARM and TI layers. Edit the
layer configuration file (`$BUILDDIR/conf/bblayers.conf`) and append the full
path to the `meta-arm-toolchain`, `meta-arm`, `meta-ti-bsp` directories to the
`BBLAYERS` variable.

## Build your first image

Now that you’re ready to start the compilation, simply run:

``` bash
bitbake core-image-minimal
```

Once the build finished, you will find the output images under
`$BUILDDIR/tmp/deploy/images/beaglebone`.

## Set up the SD card

In this first lab we will use an SD card to store the bootloader, kernel and
root filesystem files.  The SD card image has been generated and is named
`core-image-minimal-beaglebone.wic.xz`.

Now uncompress and flash the image with the following command:

``` bash
xz -dc $BUILDDIR/tmp/deploy/images/beaglebone/\
core-image-minimal-beaglebone.wic.xz | sudo dd of=/dev/sdX conv=fdatasync bs=4M
```

## Setting up serial communication with the board

The Beaglebone serial connector is exported on the 6 pins close to one of the 48
pins headers.  Using your special USB to Serial adapter provided by your
instructor, connect the ground wire (blue) to the pin closest to the power
supply connector (let’s call it pin 1), and the `TX` (red) and `RX` (green)
wires to the pins 4 (`board RX`) and 5 (`board TX`)[^2].

[^2]:See
<https://www.olimex.com/Products/Components/Cables/USB-Serial-Cable/USB-Serial-Cable-F/>
for details about the USB to Serial adapter that we are using.

You always should make sure that you connect the `TX` pin of the cable to the
`RX` pin of the board, and vice-versa, whatever the board and cables that you
use.

![Serial connection](common/beaglebone-black-serial-connection.jpg){ width=50% }

Once the USB to Serial connector is plugged in, a new serial port should appear:
`/dev/ttyUSB0`.  You can also see this device appear by looking at the output of
`dmesg`.

To communicate with the board through the serial port, install a serial
communication program, such as picocom:

``` bash
sudo apt install picocom
```

If you run `ls -l /dev/ttyUSB0`, you can also see that only root and users
belonging to the dialout group have read and write access to this
file. Therefore, you need to add your user to the dialout group:

``` bash
sudo adduser $USER dialout
```

**Important**: for the group change to be effective, in Ubuntu 18.04, you have
to completely reboot the system[^3]. A workaround is to run `newgrp dialout`, but it
is not global. You have to run it in each terminal.

[^3]: As explained on
    <https://askubuntu.com/questions/1045993/after-adding-a-group-logoutlogin-is-not-enough-in-18-04/>.

Now, you can run `picocom -b 115200 /dev/ttyUSB0`, to start serial communication
on `/dev/ttyUSB0`, with a baudrate of `115200`. If you wish to exit picocom,
press `[Ctrl][a]` followed by `[Ctrl][x]`.

There should be nothing on the serial line so far, as the board is not powered
up yet.

## Boot

Insert the SD card in the dedicated slot on the BeagleBone Black. Press the `S2`
push button (located just above the previous slot), plug in the USB cable and
release the push button. You should see boot messages on the console.

Wait until the login prompt, then enter `root` as user and as
password. Congratulations! The board has booted and you now have a shell.
