# Advanced Yocto configuration

***Configure the build, customize the output images and use NFS***

During this lab, you will:

* Customize the package selection
* Configure the build system
* Use the rootfs over NFS

## Set up the Ethernet communication and NFS on the board

Later on, we will mount our root filesystem through the network using NFS. We
will use Ethernet over USB device and therefore will only need the USB device
cable that is already used to power up the board.

First we need to set the kernel boot arguments U-Boot will pass to the Linux
kernel at boot time. To do so, mount the `bootfs` partition of the SD card on
your PC and edit the extlinux configuration file: `extlinux/extlinux.conf`.

Change the `APPEND` line to be (in just 1 line):

```bash
APPEND root=/dev/nfs rw nfsroot=192.168.0.1:/nfs,nfsvers=3,tcp ip=192.168.0.100:::::usb0
       g_ether.dev_addr=f8:dc:7a:00:00:02 g_ether.host_addr=f8:dc:7a:00:00:01
       rootwait rw console=${console},${baudrate}
```

## Set up the Ethernet communication on the workstation

To configure your network interface on the workstation side, we need to know the
name of the network interface connected to your board. You won’t be able to see
the network interface corresponding to the Ethernet over USB device connection
yet, because it’s only active when the board turns it on, from U-Boot or from
Linux. When this happens, the network interface name will be
`enx<macaddr>`. Given the value we gave to `g_ether.host_addr`, it will
therefore be `enxf8dc7a000001`.

Then, instead of configuring the host IP address from Network Manager’s
graphical interface, let’s do it through its command line interface, which is so
much easier to use:

``` bash
sudo nmcli con add type ethernet ifname enxf8dc7a000001 ip4 192.168.0.1/24
```

## Set up the NFS server on the workstation

First install the NFS server on the training computer and create the root NFS directory:

``` bash
sudo apt install nfs-kernel-server
sudo mkdir -m 777 /nfs
```

Then make sure this directory is used and exported by the NFS server by adding `/nfs *(rw,
sync,no_root_squash,subtree_check)` to the `/etc/exports` file.

Finally update the NFS server:

``` bash
sudo exportfs -r
```

## Add a package to the rootfs image

You can add packages to be built by editing the local configuration file
`$BUILDDIR/conf/local.conf`. The `IMAGE_INSTALL` variable controls the packages
included into the output image.

To illustrate this, add the Dropbear SSH server to the list of enabled packages.

Tip: do not override the default enabled package list, but append the Dropbear
package instead.

## Boot with the updated rootfs

First we need to put the rootfs under the NFS root directory so that it is
accessible by NFS clients. Simply uncompress the archived output image in the
previously created `/nfs` directory:

``` bash
sudo tar xpf $BUILDDIR/tmp/deploy/images/beaglebone/\
	core-image-minimal-beaglebone.tar.xz -C /nfs
```

Then boot the board.

The Dropbear SSH server was enabled a few steps before, and should now be running as a
service on the BeagleBone Black. You can test it by accessing the board through SSH:

``` bash
ssh root@192.168.0.100
```

You should see the BeagleBone Black command line!

## Choose a package variant

Dependencies of a given package are explicitly defined in its recipe. Some
packages may need a specific library or piece of software but others only depend
on a functionality. As an example, the kernel dependency is described by
`virtual/kernel`.

To see which kernel is used, dry-run BitBake:

``` bash
bitbake -vn virtual/kernel
```

In our case, we can see the linux-ti-staging provides the `virtual/kernel`
functionality.

**NOTE**: `selecting linux-ti-staging to satisfy virtual/kernel due to
PREFERRED_PROVIDERS`.

We can force Yocto to select another kernel by explicitly defining which one to
use in our local configuration. Try switching from `linux-ti-staging` to
`linux-dummy` only using the local configuration.

Then check the previous step worked by dry-running again BitBake.

``` bash
bitbake -vn virtual/kernel
```

You can now rebuild the whole Yocto project, with bitbake `core-image-minimal`.

**Tip**: you need to define the more specific information here to be sure it is
the one used. The `MACHINE` variable can help here.

`As` this was only to show how to select a preferred provider for a given package, you can now
use `linux-ti-staging` again.

## BitBake tips

BitBake is a powerful tool which can be used to execute specific commands. Here
is a list of some useful ones, used with the `virtual/kernel` package.

* The Yocto recipes are divided into numerous tasks, you can print them by
  using: `bitbake -c listtasks virtual/kernel`.
* BitBake allows to call a specific task only (and its dependencies) with:
  `bitbake -c <task> virtual/kernel`. (`<task>` can be menuconfig here).
* You can force to rebuild a package by calling: `bitbake -f virtual/kernel`.
* `world` is a special keyword for all packages. `bitbake --runall=fetch world`
  will download all packages sources (and their dependencies).
* You can get a list of locally available packages and their current version
  with: `bitbake -s`
* You can also find detailed information on available packages, their current
  version, dependencies or the contact information of the maintainer by
  visiting: <http://recipes.yoctoproject.org/>

For detailed information, please run `bitbake -h`

## Going further

If you have some time left, let’s improve our setup to use TFTP, in order to avoid having to
reflash the SD card for every test. What you need to do is:

1. Install a TFTP server (package `tftpd-hpa`) on your system.
1. Copy the Linux kernel image and Device Tree to the TFTP server home directory
   (specified in `/etc/default/tftpd-hpa`) so that they are made available by the
   TFTP server.
1. Change the U-Boot bootcmd to load the kernel image and the Device Tree over
   TFTP.

See the training materials bootlin's [*Embedded Linux system
development*](https://bootlin.com/fr/formation/linux-embarque/) course for
details !
