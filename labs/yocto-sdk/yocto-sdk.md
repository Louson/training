# Develop your application in the Poky SDK

***Generate and use the Poky SDK***

During this lab, you will:

* Build the Poky SDK
* Install the SDK
* Compile an application for the BeagleBone in the SDK environment

## Build the SDK

Two SDKs are available, one only embedding a toolchain and the other one
allowing for application development. We will use the latter one here.

First, build an SDK for the `slb-labs-image` image, with the `populate_sdk` task.

Once the SDK is generated, a script will be available at `tmp/deploy/sdk`.

## Install the SDK

Open a new console to be sure that no extra environment variable is set. We mean
to show you how the SDK sets up a fully working environment.

Install the SDK in `$HOME/yocto-labs/sdk` by executing the script generated at
the previous step.

``` bash
$BUILDDIR/tmp/deploy/sdk/\
poky-glibc-x86_64-slb-labs-image-cortexa8hf-neon-toolchain-2.5.sh
```

## Set up the environment

Go into the directory where you installed the SDK
(`$HOME/yocto-labs/sdk`). Source the environment script:

``` bash
source environment-setup-cortexa8hf-vfp-neon-poky-linux-gnueabi
```

Have a look at the exported environment variables:

``` bash
env
```

## Compile an application in the SDK

Download the essential Ctris sources at
<https://download.mobatek.net/sources/ctris-0.42-1-src.tar.bz2>

Extract the source in the SDK:

``` bash
tar xf ctris-0.42-1-src.tar.bz2
tar xf ctris-0.42.tar.bz2
cd ctris-0.42
```

Then modify the Makefile, to make sure that the environment variables exported
by the SDK script are not overridden.

Try to compile the application. Just like nInvaders, ctris is also an old
program and won't build with a recent toolchain. You will face these errors:

* The ctris makefile uses the native compiler, not the cross compiler provided
  by the SDK; while you could fix it using `make -e` as done for nInvaders, try
  fixing it by editing the `Makefile` this time; hint: you don't need to write
  any code, just to delete two lines
* Building with a recent GCC will give the following error, not
  reported by older versions:

  `error: format not a string literal and no format arguments [-Werror=format-security]`

  Fix this by adding `-Wno-error=format-security` to `CFLAGS`
* As for nInvaders, you will see the `multiple definition of...` error;
  add the `-fcommon` flag to `CFLAGS` also for ctris

You can check the application was successfully compiled for the right target by
using the file command. The ctris binary should be an ELF 32-bit LSB executable
compiled for ARM.

Finally, you can copy the binary to the board, by using the scp command. Then
run it and play a bit to ensure it is working fine!
