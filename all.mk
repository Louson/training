
# Needed macros
UPPERCASE = $(shell echo $1 | tr "[:lower:]-" "[:upper:]_")

include $(wildcard targets/*.mk)

ALL_TARGETS = $(sort $(basename $(notdir $(wildcard targets/*.mk))))
ALL_SLIDES = $(foreach p,$(ALL_TARGETS),$(if $($(call UPPERCASE,$(p)_SLIDES)),$(p)-slides.pdf))
ALL_INTERACTIVE = $(foreach p,$(ALL_TARGETS),$(if $($(call UPPERCASE,$(p)_SLIDES)),$(p)-slides-interactive.pdf))
ALL_LABS = $(foreach p,$(ALL_TARGETS),$(if $($(call UPPERCASE,$(p)_LABS)),$(p)-labs.pdf))
ALL_LABS_TARBALLS = $(foreach p, $(ALL_TARGETS),$(if $($(call UPPERCASE,$(p)_LABS_DATA)),$(p)-labs.tar.xz))

HELP_FIELD_FORMAT = "    %-36s %s\n"

define sep


endef

help:
	@echo "Available targets:"
	@echo -e "\n  Courses:"
	$(foreach p,$(ALL_TARGETS),\
		@printf $(HELP_FIELD_FORMAT) "$(p)-course" "Complete '$(p)' course"$(sep))
	@echo -e "\n  Slides:"
	$(foreach p,$(ALL_SLIDES),\
		@printf $(HELP_FIELD_FORMAT) "$(p)" "Slides for the '$(patsubst %-slides.pdf,%,$(p))' course"$(sep))
	@echo -e "\n  Interactive slides:"
	$(foreach p,$(ALL_INTERACTIVE),\
		@printf $(HELP_FIELD_FORMAT) "$(p)" "Interactive slides for the '$(patsubst %-slides.pdf,%,$(p))' course"$(sep))
	@echo -e "\n  Labs:"
	$(foreach p,$(ALL_LABS),\
		@printf $(HELP_FIELD_FORMAT) "$(p)" "Labs for the '$(patsubst %-labs.pdf,%,$(p))' course"$(sep))
	@echo -e "\n  Tarballs:"
	 $(foreach p,$(ALL_LABS_TARBALLS),\
	  	@printf $(HELP_FIELD_FORMAT) "$(p)" "Lab data for the '$(patsubst %-labs.tar.xz,%,$(p))' course"$(sep))

	@echo -e "\nMiscellaneous:"
	@printf $(HELP_FIELD_FORMAT) "list-courses" "List all courses"
	@printf $(HELP_FIELD_FORMAT) "help" "this help"

list-courses:
	$(foreach p,$(ALL_TARGETS),\
		@printf $(HELP_FIELD_FORMAT) "$(p)-course"$(sep))
