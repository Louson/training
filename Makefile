default: help
FORCE:

%-course: FORCE
	$(MAKE) -s -f pandoc.mk $@ TARGET=$*

#
# === Slides ===
#
%-slides.pdf: FORCE targets/%.mk
	$(MAKE) -s -f pandoc.mk $@ TARGET=$*

%-slides-interactive.pdf: FORCE targets/%.mk
	$(MAKE) -s -f pandoc.mk $*-slides.pdf TARGET=$* INTERACTIVE=1 

%-slides.tex: FORCE targets/%.mk
	$(MAKE) -s -f pandoc.mk $@ TARGET=$*

%-chapter.pdf: FORCE
	$(MAKE) -s -f pandoc.mk $@ TARGET= CHAPTER=$*

%-chapter.tex: FORCE
	$(MAKE) -s -f pandoc.mk $@ TARGET= CHAPTER=$*

#
# === Labs ===
#
%-labs.pdf: FORCE targets/%.mk
	$(MAKE) -s -f pandoc.mk $@ TARGET=$*

%-labs.tex: FORCE targets/%.mk
	$(MAKE) -s -f pandoc.mk $@ TARGET=$*

%-lab.pdf: FORCE
	$(MAKE) -s -f pandoc.mk $@ TARGET= TRAINING=$*

%-lab.tex: FORCE
	$(MAKE) -s -f pandoc.mk $@ TARGET= TRAINING=$*

#
# === Labs data ===
#
%-labs.tar.xz: FORCE
	$(MAKE) -s -f pandoc.mk $@ TARGET=$*

list-courses:
	@$(MAKE) -s -f all.mk list-courses

help:
	@$(MAKE) -s -f all.mk help

clean:
	@$(MAKE) -s -f pandoc.mk clean
