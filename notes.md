# Lab debug

## Boot

Keep S2 pushed to boot on SD card instead of eMMC.

### SD card boot

#### MMC boot

run bootcmd_legacy_mmc0

#### NFS boot MMC kernel

setenv nfsroot '10.18.0.1:/nfs,nfsvers=4,tcp'
setenv ip '10.18.0.100:10.18.0.1:10.18.0.1:255.255.255.0::eth0:off'
setenv bootargs "console=ttyS0,115200 root=/dev/nfs rw nfsroot=${nfsroot},nfsvers=4,tcp ip=${ip}"
setenv mmcboot "devnum=${mmcdev}; setenv devtype mmc; mmc rescan; run loadimage; run mmcloados"
setenv bootcmd 'run findfdt; setenv mmcdev 0; setenv bootpart 0:2; mmc dev 0; run mmcboot'

### eMMC boot
#### MMC boot
run findfdt
setenv fdtdir /boot
run mmcboot

#### NFS boot MMC kernel
run findfdt
setenv fdtdir /boot
setenv nfsroot 10.18.0.1:/nfs,nfsvers=4,tcp
setenv ip 10.18.0.100:10.18.0.1:10.18.0.1:255.255.255.0::eth0:off
setenv args_mmc 'run nfsargs'
run mmcboot


# Explainations

## Process

file.md ->(pandoc x beamer template) file.tex ->(beamer theme) file.pdf

## Why slide-level 3 ?

Document should be organized that way:
```
section
|----subsection
     |----frame
```

Refer to [pandoc
documentation](https://pandoc.org/MANUAL.html#structuring-the-slide-show) for
slide level informations.

## What is specific in our template ?

The template is based on the default template. Modifications are:

### No pause on subsection
By default, every heading above the slide level gets its own title slide. We
want it only for sections.

