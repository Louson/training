# Writing recipes - advanced

## Extending a recipe

### Introduction to recipe extensions

* It is a good practice **not** to modify recipes available in Poky.
* But it is sometimes useful to modify an existing recipe, to apply a custom
  patch for example.
* The BitBake build engine allows to modify a recipe by extending it.
* Multiple extensions can be applied to a recipe.

### Introduction to recipe extensions

* Metadata can be changed, added or appended.
* Tasks can be added or appended.
* Operators are used extensively, to add, append, prepend or assign values.

### Extend a recipe

![Extensions overview](slides/yocto-recipe-advanced/yocto-recipe-advanced-extend.pdf){ height=80% }

### Extend a recipe

* The recipe extensions end in `.bbappend`
* Append files must have the same root name as the recipe they extend, but can
  also use wildcards.
  * `example_0.1.bbappend` applies to `example_0.1.bb`
  * `example_0.%.bbappend` applies to `example_0.1.bb` and `example_0.2.bb` but
    not `example_1.0.bb`
* Append files are **version specific**. If the recipe is updated to a newer
  version, the append files must also be updated.
* If adding new files, the path to their directory must be prepended to the
  `FILESEXTRAPATHS` variable.
  * Files are looked up in paths referenced in `FILESEXTRAPATHS`, from left to
	right.
  * Prepending a path makes sure it has priority over the recipe’s one. This
	allows to override recipes’ files.


## Append file example

```bash
FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "file://custom-modification-0.patch \
	        file://custom-modification-1.patch \
		   "
```

## Advanced recipe configuration

In the real word, more complex configurations are often needed because recipes
may:

* Provide virtual packages
* Inherit generic functions from classes

### Providing virtual packages

* BitBake allows to use virtual names instead of the actual package name. We saw
  a use case with *package variants*.
* The virtual name is specified through the `PROVIDES` variable.
* Several recipes can provide the same virtual name. Only one will be built and
  installed into the generated image.
* `PROVIDES = "virtual/kernel"`

## Classes

### Introduction to classes

* Classes provide an abstraction to common code, which can be re-used in
  multiple recipes.
* Common tasks do not have to be re-developed!
* Any metadata and task which can be put in a recipe can be used in a class.
* Classes extension is `.bbclass`
* Classes are located in the `classes` folder of a layer.
* Recipes can use this common code by inheriting a class:  
  `inherit <class>`

### Common classes

Common classes can be found in `meta/classes/`:

* `base.bbclass`
* `kernel.bbclass`
* `autotools.bbclass`
* `autotools-brokensep.bbclass`
* `cmake.bbclass`
* `native.bbclass`
* `systemd.bbclass`
* `update-rc.d.bbclass`
* `useradd.bbclass`
* ...

### The base class

* Every recipe inherits the base class automatically.
* Contains a set of basic common tasks to fetch, unpack or compile applications.
* Inherits other common classes, providing:
* Mirrors definitions: `DEBIAN_MIRROR`, `GNU_MIRROR`, `KERNELORG_MIRROR`...
* Automatic application of patch files listed in `SRC_URI`
* Some tasks: `clean`, `listtasks` or `fetch`.
* Defines `oe_runmake`, using `EXTRA_OEMAKE` to use custom arguments.

### The kernel class

* Used to build Linux kernels.
* Defines tasks to configure, compile and install a kernel and its modules.
* The kernel is divided into several packages: `kernel`, `kernel-base`,
  `kernel-dev`, `kernel-modules`...
* Automatically provides the virtual package `virtual/kernel` .
* Configuration variables are available:
  * `KERNEL_IMAGETYPE`, defaults to `zImage`
  * `KERNEL_EXTRA_ARGS`
  * `INITRAMFS_IMAGE`

### The autotools class

* Defines tasks and metadata to handle applications using the autotools build
  system (autoconf, automake and libtool):
* `do_configure`: generates the configure script using `autoreconf` and loads it
  with standard arguments or cross-compilation.
* `do_compile`: runs `make`
* `do_install`: runs `make install`
* Extra configuration parameters can be passed with `EXTRA_OECONF`.
* Compilation flags can be added thanks to the `EXTRA_OEMAKE` variable.

### Example: use the autotools class

```bash
DESCRIPTION = "Print a friendly, customizable greeting"
HOMEPAGE = "https://www.gnu.org/software/hello/"
PRIORITY = "optional"
SECTION = "examples"
LICENSE = "GPL-3.0-or-later"

SRC_URI = "${GNU_MIRROR}/hello/hello-${PV}.tar.gz"
SRC_URI[md5sum] = "67607d2616a0faaf5bc94c59dca7c3cb"
SRC_URI[sha256sum] = "ecbb7a2214196c57ff9340aa71458e1559abd38f6d8d169666846935df191ea7"
LIC_FILES_CHKSUM = "file://COPYING;md5=d32239bcb673463ab874e80d47fae504"

inherit autotools
```

### The useradd class

* This class helps to add users to the resulting image.
* Adding custom users is required by many services to avoid running them as root.
* `USERADD_PACKAGES` must be defined when the `useradd` class is
  inherited. Defines the list of packages which needs the user.
* Users and groups will be created before the packages using it perform their
  `do_install`.
* At least one of the two following variables must be set:
  * `USERADD_PARAM`: parameters to pass to `useradd`.
  * `GROUPADD_PARAM`: parameters to pass to `groupadd`.

### Example: the useradd class

```bash
DESCRIPTION = "useradd class usage example"
PRIORITY = "optional"
SECTION = "examples"
LICENSE = "MIT"

SRC_URI = "file://file0"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/MIT;md5=0835ade698e0bc..."

inherit useradd

USERADD_PACKAGES = "${PN}"
USERADD_PARAM = "-u 1000 -d /home/user0 -s /bin/bash user0"

do_install() {
	install -m 644 file0 ${D}/home/user0/
	chown user0:user0 ${D}/home/user0/file0
}
```

## Binary packages

### Specifics for binary packages

* It is possible to install binaries into the generated root filesystem.
* Set the `LICENSE` to `CLOSED`.
* Use the `do_install` task to copy the binaries into the root file system.

## Bitbake file inclusions

### Locate files in the build system

* Metadata can be shared using included files.
* BitBake uses the `BBPATH` to find the files to be included. It also looks into the
  current directory.
* Three keywords can be used to include files from recipes, classes or other
  configuration files:
  * `inherit`
  * `include`
  * `require`

### The `inherit` keyword

* `inherit` can be used in recipes or classes, to inherit the functionalities of a class.
* To inherit the functionalities of the `kernel` class, use: `inherit kernel`
* `inherit` looks for files ending in `.bbclass`, in classes directories found
  in `BBPATH`.
* It is possible to include a class conditionally using a variable:  
  `inherit ${FOO}`

### The `include` and `require` keywords

* `include` and `require` can be used in all files, to insert the content of another file
  at that location.
* If the path specified on the `include` (or `require`) path is relative, BitBake will
  insert the first file found in `BBPATH`.
* `include` does not produce an error when a file cannot be found, whereas require
  raises a parsing error.
* To include a local file: `include ninvaders.inc`
* To include a file from another location (which could be in another layer):  
  `require path/to/file.inc`

## Debugging recipes

### Debugging recipes

* For each task, logs are available in the `temp` directory in the work folder of
  a recipe. This includes both the actual tasks code that ran and the output of
  the task.
* BitBake can dump the whole environment, including the variable values and how
  they were set:
  ```bash
  $ bitbake -e ninvaders
  # $DEPENDS [4 operations]
  # set /yocto-labs/poky/meta/conf/bitbake.conf:268
  # ""
  # set /yocto-labs/poky/meta/conf/documentation.conf:130
  # [doc] "Lists a recipe's build-time dependencies (i.e. other recipe files)."
  # :prepend /yocto-training/yocto-labs/poky/meta/classes/base.bbclass:74
  # "${BASEDEPENDS} "
  # set /yocto-labs/meta-bootlinlabs/recipes-games/ninvaders/ninvaders.inc:11
  # "ncurses"
  # pre-expansion value:
  # "${BASEDEPENDS} ncurses"
  ...
  ```

### Debugging recipes

* A development shell, exporting the full environment can be used to debug build
  failures:
  ```bash
  $ bitbake -c devshell <recipe>
  ```
* To understand what a change in a recipe implies, you can activate build history in
  `local.conf`:
  ```bash
  INHERIT += "buildhistory"
  BUILDHISTORY_COMMIT = "1"
  ```

  Then use the buildhistory-diff tool to examine differences between two
  builds:  
  * `./scripts/buildhistory-diff`

## Network usage

### Source fetching

* BitBake will look for files to retrieve at the following locations, in order:
  1. `DL_DIR` (the local download directory).
  1. The `PREMIRRORS` locations.
  1. The upstream source, as defined in `SRC_URI`.
  1. The `MIRRORS` locations.
* If all the mirrors fail, the build will fail.

### Mirror configuration in OpenEmbedded-Core

`meta/classes/mirrors.bbclass`

```bash
PREMIRRORS += "git://sourceware.org/git/glibc.git        https://downloads.yoctoproject.org/mirror/sources/ \
               git://sourceware.org/git/binutils-gdb.git https://downloads.yoctoproject.org/mirror/sources/"

MIRRORS += "\
svn://.*/.*     http://sources.openembedded.org/ \
git://.*/.*     http://sources.openembedded.org/ \
https?://.*/.*  http://sources.openembedded.org/ \
ftp://.*/.*     http://sources.openembedded.org/ \
...
"
```

### Configuring the premirrors

* It is easy to add a custom mirror to the `PREMIRRORS` by using the
  `own-mirrors` class (only one URL supported):
  ```bash
  INHERIT += "own-mirrors"
  SOURCE_MIRROR_URL = "http://example.com/my-mirror"
  ```

* For a more complex setup, prepend custom mirrors to the `PREMIRRORS` variable:
  ```bash
  PREMIRRORS:prepend = "\
	  git://.*/.* http://www.yoctoproject.org/sources/ \n \
	  ftp://.*/.* http://www.yoctoproject.org/sources/ \n \
	  http://.*/.* http://www.yoctoproject.org/sources/ \n \
	  https://.*/.* http://www.yoctoproject.org/sources/ \n"
  ```

### Forbidding network access

* You can use `BB_GENERATE_MIRROR_TARBALLS = "1"` to generate tarballs of the git
  repositories in `DL_DIR`
* You can also completely disable network access using  
  `BB_NO_NETWORK = "1"`
  * To download all the sources before disabling network access use  
    `bitbake --runall=fetch core-image-minimal`
* Or restrict BitBake to only download files from the `PREMIRRORS`, using  
  `BB_FETCH_PREMIRRORONLY = "1"`
