# Distro layers

![Distro layers](slides/yocto-layer-distro/yocto-layer-distro.pdf){ height=80% }

### Distro layers

* You can create a new distribution by using a Distro layer.
* This allows to change the defaults that are used by Poky.
* It is useful to distribute changes that have been made in `local.conf`

### Best practice

* A distro layer is used to provide policy configurations for a custom
  distribution.
* It is a best practice to separate the distro layer from the custom layers you
  may create and use.
* It often contains:
  * Configuration files.
  * Specific classes.
  * Distribution specific recipes: initialization scripts, splash screen...

### Creating a Distro layer

* The configuration file for the distro layer is `conf/distro/<distro>.conf`
* This file must define the `DISTRO` variable.
* It is possible to inherit configuration from an existing distro layer.
* You can also use all the `DISTRO_*` variables.
* Use `DISTRO = "<distro>"` in `local.conf` to use your distro configuration.

``` bash
require conf/distro/poky.conf

DISTRO = "distro"
DISTRO_NAME = "distro description"
DISTRO_VERSION = "1.0"

MAINTAINER = "..."
```

### `DISTRO_FEATURES`

* Lists the features the distribution will provide.
* As for `MACHINE_FEATURES`, this is used by package recipes to enable or
  disable functionalities.
* `COMBINED_FEATURES` provides the list of features that are enabled in both
  `MACHINE_FEATURES` and `DISTRO_FEATURES`.

### Toolchain selection

* The toolchain selection is controlled by the `TCMODE` variable.
* It defaults to `"default"`.
* The `conf/distro/include/tcmode-${TCMODE}.inc` file is included.
  * This configures the toolchain to use by defining preferred providers and
	versions for recipes such as `gcc`, `binutils`, `*libc`...
* The providers’ recipes define how to compile or/and install the toolchain.
* Toolchains can be built by the build system or external.

### Sample files

* A distro layer often contains sample files, used as templates to build key
  configurations files.
* Example of sample files:
  * `bblayers.conf.sample`
  * `local.conf.sample`
* In Poky, they are in `meta-poky/conf/`.
* The `TEMPLATECONF` variable controls where to find the samples.
* It is set in `${OEROOT}/.templateconf`.

## Release management

### Release management

There are multiple tasks that OE/bitbake based projects let you do on your own
to ensure build reproducibility:

* Code distribution and project setup.
* Release tagging

A separate tool is needed for that, usual solutions are:

* `combo-layer`, as done by Poky:
  <https://wiki.yoctoproject.org/wiki/Combo-layer>
* `git submodules` + setup script. Great example in YOE:
  <https://github.com/YoeDistro/yoe-distro>
* `repo` and `templateconf` or setup script
* `kas`

### Distribute the distribution

* A good way to distribute a distribution (Poky, custom layers, BSP,
  `.templateconf`...) is to use Google’s `repo`.
* Repo is used in Android to distribute its source code, which is split into
  many git repositories. It’s a wrapper to handle several git repositories at
  once.
* The only requirement is to use git.
* The repo configuration is stored in manifest file, usually available in its
  own git repository.
* It could also be in a specific branch of your custom layer.
* It only handles fetching code, handling `local.conf` and `bblayers.conf` is done
  separately

### Manifest example

```xml
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
  <remote name="yocto-project" fetch="git.yoctoproject.org" />
  <remote name="private" fetch="git.example.net" />

  <default revision="kirkstone" remote="private" />

  <project name="poky" remote="yocto-project" />
  <project name="meta-ti-bsp" remote="yocto-project" />
  <project name="meta-custom" />
  <project name="meta-custom-bsp" />
  <project path="meta-custom-distro" name="distro">
	<copyfile src="templateconf" dest="poky/.templateconf" />
  </project>
</manifest>
```

### Retrieve the project using repo

``` bash
$ mkdir my-project; cd my-project
$ repo init -u https://git.example.net/manifest.git
$ repo sync -j4
```

* `repo init` uses the `default.xml` manifest in the repository, unless specified
  otherwise.
* You can see the full repo documentation at
  <https://source.android.com/source/using-repo.html>.

### repo: release

To tag a release, a few steps have to be taken:

* Optionally tag the custom layers
* For each project entry in the manifest, set the revision parameter to either a
  tag or a commit hash.
* Commit and tag this version of the manifest.

### kas

* Specific tool developed by Siemens for OpenEmbedded:
  <https://github.com/siemens/kas>
* Will fetch layers and build the image in a single command
* Uses a single JSON or YAML configuration file part of the custom layer
* Can generate and run inside a Docker container
* Can setup `local.conf` and `bblayers.conf`

### kas configuration

```yaml
header:
  version: 8
machine: mymachine
distro: mydistro
target:
  - myimage

repos:
  meta-custom:
  bitbake:
    url: "https://git.openembedded.org/bitbake"
	refspec: "2.0"
    layers:
	  .: excluded

  openembedded-core:
    url: "https://git.openembedded.org/openembedded-core"
	refspec: kirkstone
	layers:
	  meta:
```

### kas configuration

```yaml
meta-freescale:
  url: "https://github.com/Freescale/meta-freescale"
  refspec: kirkstone

meta-openembedded:
  url: https://git.openembedded.org/meta-openembedded
  refspec: kirkstone
  layers:
	meta-oe:
	meta-python:
	meta-networking:
```

* Then a single command will build all the listed target for the machine:  
  `$ kas build meta-custom/mymachine.yaml`
* Or, alternatively, invoke bitbake commands:  
  `$ kas shell /path/to/kas-project.yml -c 'bitbake dosfsutils-native'`

