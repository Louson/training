# Licensing

## Managing licences

### Tracking license changes

* The license of an external project may change at some point.
* The `LIC_FILES_CHKSUM` tracks changes in the license files.
* If the license’s checksum changes, the build will fail.
  * The recipe needs to be updated.

``` bash
LIC_FILES_CHKSUM = " \
file://COPYING;md5=... \
file://src/file.c;beginline=3;endline=21;md5=..."
```

* `LIC_FILES_CHKSUM` is mandatory in every recipe, unless `LICENSE` is set to `CLOSED`.

### Package exclusion

* We may not want some packages due to their licenses.
* To exclude a specific license, use `INCOMPATIBLE_LICENSE`
* To exclude all GPLv3 packages:
  ``` bash
  INCOMPATIBLE_LICENSE = "GPL-3.0* LGPL-3.0* AGPL-3.0*"
  ```
* License names are the ones used in the LICENSE variable.
* The `meta-gplv2` layer provides recipes for software where upstream has moved to
  GPLv3 licenses.

### Commercial licenses

* By default the build system does not include commercial components.
* Packages with a commercial component define:
  ``` bash
  LICENSE_FLAGS = "commercial"
  ```
* To build a package with a commercial component, the package must be in the
  `LICENSE_FLAGS_ACCEPTED` variable.
* Example, gst-plugins-ugly:
  ``` bash
  LICENSE_FLAGS_ACCEPTED = "commercial_gst-plugins-ugly"
  ```

### Listing licenses

OpenEmbbedded will generate a manifest of all the licenses of the software
present on the target image in
`$BUILDDIR/tmp/deploy/licenses/<image>/license.manifest`

``` bash
PACKAGE NAME: busybox
PACKAGE VERSION: 1.31.1
RECIPE NAME: busybox
LICENSE: GPLv2 & bzip2-1.0.6

PACKAGE NAME: dropbear
PACKAGE VERSION: 2019.78
RECIPE NAME: dropbear
LICENSE: MIT & BSD-3-Clause & BSD-2-Clause & PD
```

### Providing license text

To include the license text in the root filesystem either:

* Use `COPY_LIC_DIRS = "1"` and `COPY_LIC_MANIFEST = "1"`
* or use `LICENSE_CREATE_PACKAGE = "1"` to generate packages including the
  license and install the required license packages.

### Providing sources

OpenEmbbedded provides the `archiver` class to generate tarballs of the source
code:

* Use `INHERIT += "archiver"`
* Set the `ARCHIVER_MODE` variable, the default is to provide patched
  sources. To provide configured sources:
  ``` bash
  ARCHIVER_MODE[src] = "configured"
  ```

