# Yocto Project Resources

Yocto Project documentation
* https://docs.yoctoproject.org/
* Wiki: https://wiki.yoctoproject.org/wiki/Main_Page
* https://layers.openembedded.org/

### Useful Reading (1)

Embedded Linux Development with Yocto Project - Second Edition, Nov 2017

* [packtub](https://www.packtpub.com/virtualization-and-cloud/embedded-linux-development-using-yocto-projects-second-edition)
* By Otavio Salvador and Daiane Angolini
* From basic to advanced usage, helps writing better, more flexible recipes. A
  good reference to jumpstart your Yocto Project development.

### Useful Reading (2)

Embedded Linux Projects Using Yocto Project Cookbook - Second Edition, January
2018

* [Packtub](https://www.packtpub.com/virtualization-and-cloud/embedded-linux-development-using-yocto-project-cookbook-second-edition)
* By Alex González
* A set of recipes that you can refer to and solve your immediate problems
  instead of reading it from cover to cover.

