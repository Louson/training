# Introduction to Embedded Linux

### Simplified Linux system architecture

![Linux system
architecture](slides/embeddedlinux/linux-system-architecture.pdf){ height=80% }

### Overall Linux boot sequence

![Overall boot sequence](slides/embeddedlinux/overall-boot-sequence.pdf){
height=80% }

### Embedded Linux work

* **BSP work**: porting the bootloader and Linux kernel, developing Linux device
  drivers.
* **system integration work**: assembling all the user space components needed for
  the system, configure them, develop the upgrade and recovery mechanisms, etc.
* **application development**: write the company-specific applications and
  libraries.

### Complexity of user space integration

![Complexity of user space
integration](slides/embeddedlinux/pacgraph.pdf){ height=70% width=100% }

### System integration: several possibilites 1/3

#### Building everything manually

| Pros                  | Cons                                  |
| :---                  | :---                                  |
| - Full flexibility    | - Dependency hell                     |
| - Learning experience | - Need to understand a lot of details |
|                       | - Version compatibility               |
|                       | - Lack of reproducibility             |
|                       |                                       |

### System integration: several possibilites 2/3

#### Binary distribution (Debian, Ubuntu, Fedora, etc)

| Pros                        | Cons                                             |
| :---                        | :---                                             |
| - Easy to create and extend | - Hard to customize                              |
|                             | - Hard to optimize (boot time, size)             |
|                             | - Hard to rebuild the full system from source    |
|                             | - Large system                                   |
|                             | - Uses native compilation (slow)                 |
|                             | - No well-defined mechanism to generate an image |
|                             | - Lots of mandatory dependencies                 |
|                             | - Not available for all architectures            |
|                             |                                                  |

### System integration: several possibilites 3/3

#### Build systems (Buildroot, Yocto, PTXdist, etc.)

| Pros                                   | Cons                                   |
| :---                                   | :---                                   |
| - Nearly full flexibility              | - Not as easy as a binary distribution |
| - Built from source: customization and | - Build time                           |
| optimization are easy                  |                                        |
| - Fully reproducible                   |                                        |
| - Uses cross-compilation               |                                        |
| - Have embedded specific packages not  |                                        |
| necessarily in desktop distros         |                                        |
| - Make more features optional          |                                        |
|                                        |                                        |

<!-- | Pros                                                               | Cons                                 | -->
<!-- | :---                                                               | :---                                 | -->
<!-- | Nearly full flexibility                                            | Not as easy as a binary distribution | -->
<!-- | Built from source: customization and optimization are easy         | Build time                           | -->
<!-- | Fully reproducible                                                 |                                      | -->
<!-- | Uses cross-compilation                                             |                                      | -->
<!-- | Have embedded specific packages not necessarily in desktop distros |                                      | -->
<!-- | Make more features optional                                        |                                      | -->
<!-- |                                                                    |                                      | -->

### Embedded Linux build system: principle

![Build system principle](slides/embeddedlinux/buildsystem-principle.pdf){
height=50% }

* Building from source → lot of flexibility
* Cross-compilation → leveraging fast build machines
* Recipes for building components → easy

### Embedded Linux build system: tools


* A wide range of solutions: Yocto/OpenEmbedded, PTXdist, Buildroot,
OpenWRT, and more.
* Today, two solutions are emerging as the most popular ones
  * **Yocto/OpenEmbedded** Builds a complete Linux distribution with binary
    packages. Powerful, but somewhat complex, and quite steep learning curve.
  * **Buildroot** Builds a root filesystem image, no binary packages. Much
	simpler to use, understand and modify.
