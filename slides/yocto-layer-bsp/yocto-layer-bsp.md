# BSP layers

## Introduction to BSP layers in the Yocto Project

### BSP layers

![BSP overview](slides/yocto-layer-bsp/yocto-bsp-overview.pdf){ height=80% }

### Overview

* BSP layers are device specific layers. They hold metadata with the purpose of
  supporting specific hardware devices.
* BSP layers describe the hardware features and often provide a custom kernel
  and bootloader with the required modules and drivers.
* BSP layers can also provide additional software, designed to take advantage of
  the hardware features.
* As a layer, it is integrated into the build system as we previously saw.
* A good practice is to name it `meta-<bsp_name>`.

### BSP layers Specifics

* BSP layers are a subset of the layers.
* In addition to package recipes and build tasks, they often provide:
  * Hardware configuration files (`machines`).
  * Bootloader, kernel and display support and configuration.
  * Pre-built user binaries.

## Hardware configuration files

### Overview 1/2

* A layer provides one machine file (hardware configuration file) per machine it
  supports.
* These configuration files are stored under `meta-<bsp_name>/conf/machine/*.conf`
* The file names correspond to the values set in the `MACHINE` configuration variable.
  * `meta-ti-bsp/conf/machine/beaglebone.conf`
  * `MACHINE = "beaglebone"`
* Each machine should be described in the `README` file of the BSP.

### Overview 2/2

* The hardware configuration file contains configuration variables related to
  the architecture and to the machine features.
* Some other variables help customize the kernel image or the filesystems used.

### Machine configuration

`TARGET_ARCH`
: The architecture of the device being built.

`PREFERRED_PROVIDER_virtual/kernel`
: The default kernel.

`MACHINE_FEATURES`
: List of hardware features provided by the machine, e.g. `usbgadget` `usbhost`
  `screen` `wifi`

`SERIAL_CONSOLES`
: Speed and device for the serial console to attach. Used to configure `getty`,
  e.g. `115200;ttyS0`

`KERNEL_IMAGETYPE`
: The type of kernel image to build, e.g. `zImage`

### MACHINE_FEATURES

* Lists the hardware features provided by the machine.
* These features are used by package recipes to enable or disable functionalities.
* Some packages are automatically added to the resulting root filesystem
  depending on the feature list.
* The feature `bluetooth`:
  * Asks the `bluez` daemon to be built and added to the image.
  * Enables bluetooth support in `ConnMan`.

### conf/machine/include/cfa10036.inc

``` bash
# Common definitions for cfa-10036 boards
include conf/machine/include/imx-base.inc
include conf/machine/include/tune-arm926ejs.inc

SOC_FAMILY = "mxs:mx28:cfa10036"

PREFERRED_PROVIDER_virtual/kernel ?= "linux-cfa"
PREFERRED_PROVIDER_virtual/bootloader ?= "barebox"
IMAGE_BOOTLOADER = "barebox"
BAREBOX_BINARY = "barebox"
IMAGE_FSTYPES:mxs = "tar.bz2 barebox.mxsboot-sdcard sdcard.gz"
IMXBOOTLETS_MACHINE = "cfa10036"

KERNEL_IMAGETYPE = "zImage"
KERNEL_DEVICETREE = "imx28-cfa10036.dtb"
# we need the kernel to be installed in the final image
IMAGE_INSTALL_append = " kernel-image kernel-devicetree"
SDCARD_ROOTFS ?= "${DEPLOY_DIR_IMAGE}/${IMAGE_NAME}.rootfs.ext3"
SERIAL_CONSOLES = "115200 ttyAMA0"
MACHINE_FEATURES = "usbgadget usbhost vfat"
```

### conf/machine/cfa10057.conf

``` bash
#@TYPE: Machine
#@NAME: Crystalfontz CFA-10057
#@SOC: i.MX28
#@DESCRIPTION: Machine configuration for CFA-10057, also called CFA-920
#@MAINTAINER: Alexandre Belloni <alexandre.belloni@bootlin.com>

include conf/machine/include/cfa10036.inc

KERNEL_DEVICETREE += "imx28-cfa10057.dtb"

MACHINE_FEATURES += "touchscreen"
```

## Bootloader

### Default bootloader 1/2

* By default the bootloader used is the mainline version of U-Boot, with a fixed
  version (per Poky release).
* All the magic is done in `meta/recipes-bsp/u-boot/u-boot.inc`
* Some configuration variables used by the U-Boot recipe can be customized, in the
  machine file.

### Default bootloader 2/2

`SPL_BINARY`
: If an SPL is built, describes the name of the output binary. Defaults to an
  empty string.

`UBOOT_SUFFIX`
: `bin` (default) or `img`.

`UBOOT_MACHINE`
: The target used to build the configuration.

`UBOOT_ENTRYPOINT`
: The bootloader entry point.

`UBOOT_LOADADDRESS`
: The bootloader load address.

`UBOOT_MAKE_TARGET`
: Make target when building the bootloader. Defaults to all.

### Customize the bootloader

* It is possible to support a custom U-Boot by creating an extended recipe and
  to append extra metadata to the original one.
* This works well when using a mainline version of U-Boot.
* Otherwise it is possible to create a custom recipe.
  * Try to still use `meta/recipes-bsp/u-boot/u-booot.inc`

## Kernel

### Linux kernel recipes in Yocto

* There are mainly two ways of compiling a kernel:
  * By creating a custom kernel recipe, inheriting `kernel.bbclass`
  * By using the `linux-yocto` packages, provided in Poky, for very complex needs
* The kernel used is selected in the machine file thanks to:
  `PREFERRED_PROVIDER_virtual/kernel`
* Its version is defined with: `PREFERRED_VERSION_<kernel_provider>`

### Linux Yocto 1/3

* `linux-yocto` is a set of recipes with advanced features to build a mainline
  kernel
* `PREFERRED_PROVIDER_virtual/kernel = "linux-yocto"`
* `PREFERRED_VERSION_linux-yocto = "5.14\%"`

### Linux Yocto 2/3

* Automatically applies a `defconfig` listed in `SRC_URI`
+ Automatically applies configuration fragments listed in `SRC_URI` with a
  `.cfg` extension

``` bash
SRC_URI += "file://defconfig \
	file://nand-support.cfg \
	file://ethernet-support.cfg"
```

### Linux Yocto 3/3

* Another way of configuring linux-yocto is by using Advanced Metadata.
* It is a powerful way of splitting the configuration and the patches into
  several pieces.
* It is designed to provide a very configurable kernel, at the cost of higher
  complexity
* The full documentation can be found at
  <https://docs.yoctoproject.org/kernel-dev/advanced.html#working-with-advanced-metadata-yocto-kernel-cache>

### Linux Yocto: Kernel Metadata 1/2

* Kernel Metadata is a way to organize and to split the kernel configuration and
  patches in little pieces each providing support for one feature.
* Two main configuration variables help taking advantage of this:

  `LINUX_KERNEL_TYPE`
  : `standard` (default), `tiny` or `preempt-rt`
	* `standard`: generic Linux kernel policy.
	* `tiny`: bare minimum configuration, for small kernels.
	* `preempt-rt`: applies the PREEMPT_RT patch.

  `KERNEL_FEATURES`
  : List of features to enable. Features are sets of patches and configuration
  fragments.

### Linux Yocto: Kernel Metadata 2/2

* Kernel Metadata description files have their own syntax to describe an
  optional kernel feature
* A basic feature is defined as a patch to apply and a configuration fragment
    to add
* Simple example, `features/smp.scc`
  ``` bash
  define KFEATURE_DESCRIPTION "Enable SMP"
  kconf hardware smp.cfg
  patch smp-support.patch
  ```
* To integrate the feature into the kernel image:  
  `KERNEL_FEATURES += "features/smp.scc"`
