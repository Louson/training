# Writing recipes - going further

## Using Python code in metadata

### Tasks in Python

* Tasks can be written in Python when using the keyword `python`.
* Two modules are automatically imported:
  * `bb`: to access BitBake's internal functions.
  * `os`: Python's operating system interfaces.
* You can import other modules using the keyword `import`.
* Anonymous Python functions are executed during parsing.
* Short Python code snippets can be written inline with the `${@<python code>}`
  syntax.

### Accessing the datastore with Python

* The `d` variable is accessible within Python tasks.
* `d` represents the BitBake datastore (where variables are stored).

`d.getVar("X", expand=False)`
: Returns the value of `X`.

`d.setVar("X", "value")`
: Set `X`.

`d.appendVar("X", "value")`
: Append `value` to `X`.

`d.prependVar("X", "value")`
: Prepend `value` to `X`.

`d.expand(expression)`
: Expand variables in `expression`.

### Python code example

``` python
# Anonymous function
python __anonymous() {
    if d.getVar("FOO", True) == "example":
        d.setVar("BAR", "Hello, World.")
}

# Task
python do_settime() {
    import time

    d.setVar("TIME", time.strftime('%Y%m%d', time.gmtime()))
}
```

```bash
# Inline
do_install() {
	echo "Build OS: ${@os.uname()[0].lower()}"
}
```

## Variable flags

### Variable flags

* *Variable flags* are used to store extra information on tasks and variables.
  `SRC_URI[md5sum] = "97b2c3fb082241ab5c56ab728522622b"`
* They are used to control task functionalities.
* A number of these flags are already used by BitBake:
    * `dirs`: directories that should be created before the task runs. The last
      one becomes the work directory for the task.
    * `noexec`: disable the execution of the task.
    * `nostamp`: do not create a *stamp* file when running the task. The task
      will always be executed.
    * `doc`: task documentation displayed by `listtasks`.

``` bash
do_menuconfig[nostamp] = "1"
do_settime[noexec] = "1"
do_settime[doc] = "Set the current time in ${TIME}"
```

## Packages features

### Benefits

* Features can be built depending on the needs.
* This allows to avoid compiling all features in a software component when only
  a few are required.
* A good example is `ConnMan`: Bluetooth support is built only if there is
  Bluetooth on the target.
* The `PACKAGECONFIG` variable is used to configure the build on a per feature
  granularity, for packages.


### `PACKAGECONFIG`

* `PACKAGECONFIG` takes the list of features to enable.
* `PACKAGECONFIG[feature]` takes up to six arguments, separated by commas:
  1.  Argument used by the configuration task if the feature is enabled (`EXTRA_OECONF`).
  1.  Argument added to `EXTRA_OECONF` if the feature is disabled.
  1.  Additional build dependency (`DEPENDS`), if enabled.
  1.  Additional runtime dependency (`RDEPENDS`), if enabled.
  1.  Additional runtime recommendations (`RRECOMMANDS`), if enabled.
  1.  Any conflicting `PACKAGECONFIG` settings for this feature.
* Unused arguments can be omitted or left blank.

### Example: from `ConnMan`

``` bash
PACKAGECONFIG ??= "wifi openvpn"

PACKAGECONFIG[wifi] = "--enable-wifi,                 \
                       --disable-wifi,                \
                       wpa-supplicant,                \
                       wpa-supplicant"
PACKAGECONFIG[bluez] = "--enable-bluetooth,           \
                        --disable-bluetooth,          \
                        bluez5,                       \
                        bluez5"
PACKAGECONFIG[openvpn] = "--enable-openvpn,           \
                          --disable-openvpn,          \
                          ,                           \
                          openvpn"
```



## Conditional features

### Conditional features

* Some values can be set dynamically, thanks to a set of functions:
* `bb.utils.contains(variable, checkval, trueval, falseval, d)`: if `checkval`
  is found in `variable`, `trueval` is returned; otherwise `falseval` is used.
* `bb.utils.filter(variable, checkvalues, d)`: returns all the words in the
   variable that are present in the checkvalues.
* Example:
  ``` sh
  PACKAGECONFIG ??= "wispr iptables client\
                     ${@bb.utils.filter('DISTRO_FEATURES', '3g systemd wifi', d)} \
                     ${@bb.utils.contains('DISTRO_FEATURES', 'bluetooth', 'bluez', '', d)} \
                     "
  ```

## Root filesystem creation

### Root filesystem generation

* Image generation overview:
  1. An empty directory is created for the root filesystem.
  1. Packages from `IMAGE_INSTALL` are installed into it using the package
	 manager.
  1.  One or more images files are created, depending on the `IMAGE_FSTYPES` value.
* The rootfs creation is specific to the `IMAGE_PKGTYPE` value. It should be
  defined in the image recipe, otherwise the first valid package type defined in
  `PACKAGE_CLASSES` is used.
* All the magic is done in `meta/classes/rootfs_${IMAGE_PKGTYPE}.bbclass`

## Package splitting

### Package splitting 1/2

![Package splitting](slides/yocto-recipe-extra/splitting-packages.pdf){
height=60% }

### Package splitting 2/2

* `do_install` copies **all** files in the `D` directory (`${WORKDIR}/image`).
* `do_package` splits files in `${WORKDIR}/packages_split`.
  * based on the `PACKAGES` and `FILES` variables.
* `do_package_rpm` generates RPM package.

### `PACKAGES`

* `PACKAGES` lists the packages to be built:
  ``` bash
  PACKAGES = "${PN}-src ${PN}-dbg ${PN}-staticdev ${PN}-dev \
      ${PN}-doc ${PN}-locale ${PACKAGE_BEFORE_PN} ${PN}"
  ```
* More packages can be added to the default list
  * Useful when a single remote repository provides multiple binaries or
	libraries
  * The order matters. `PACKAGE_BEFORE_PN` allows to pick files normally
	included in the default package in another.
* `PACKAGES_DYNAMIC` allows to check if dependencies with optional packages are
  satisfied.
* `ALLOW_EMPTY` allows to produce a package even if it is empty.
* To prevent configuration files to be overwritten during the Package Management
  System update process, use `CONFFILES`.

### `FILES`

* For each package, a `FILES` variable lists the files to include
  * It must be package specific (e.g. with `:${PN}`, `:${PN}-dev`, dots)
  * Defaults from `meta/conf/bitbake.conf`:
  ``` bash
  FILES:${PN}-dev = \
      "${includedir} ${FILES_SOLIBSDEV} ${libdir}/*.la \
       ${libdir}/*.o ${libdir}/pkgconfig ${datadir}/pkgconfig \
       ${datadir}/aclocal ${base_libdir}/*.o \
       ${libdir}/${BPN}/*.la ${base_libdir}/*.la \
       ${libdir}/cmake ${datadir}/cmake"
  FILES:${PN}-dbg = \
      "/usr/lib/debug /usr/lib/debug-static \
       /usr/src/debug"
  ```

### `FILES`: the main package

* The package named just `${PN}` is the one that gets installed in the root
  filesystem.
* In Poky, defaults to:
  ```bash
  "${bindir}/* ${sbindir}/* ${libexecdir}/* ${libdir}/lib*${SOLIBS} \
  ${sysconfdir} ${sharedstatedir} ${localstatedir} \
     ${base_bindir}/* ${base_sbindir}/* \
     ${base_libdir}/*${SOLIBS} \
     ${base_prefix}/lib/udev/rules.d ${prefix}/lib/udev/rules.d \
     ${datadir}/${BPN} ${libdir}/${BPN}/* \
     ${datadir}/pixmaps ${datadir}/applications \
     ${datadir}/idl ${datadir}/omf ${datadir}/sounds \
     ${libdir}/bonobo/servers"
  ```

### Example

* The `kexec-tools` provides `kexec` and `kdump`:
  ``` bash
   require kexec-tools.inc
   export LDFLAGS = "-L${STAGING_LIBDIR}"
   EXTRA_OECONF = " --with-zlib=yes"

   SRC_URI[md5sum] = \
       "b9f2a3ba0ba9c78625ee7a50532500d8"
   SRC_URI[sha256sum] = "..."

   PACKAGES =+ "kexec kdump"

   FILES:kexec = "${sbindir}/kexec"
   FILES:kdump = "${sbindir}/kdump"
   ```

### Inspecting packages

`oe-pkgdata-util` is a tool that can help inspecting packages:

* Which package is shipping a file:
  ``` bash
  $ oe-pkgdata-util find-path /bin/busybox
  busybox: /bin/busybox
  ```
* Which files are shipped by a package:
  ``` bash
  $ oe-pkgdata-util list-pkg-files busybox
  busybox:
      /bin/busybox
      /bin/busybox.nosuid
      /bin/busybox.suid
      /bin/sh
  ```
* Which recipe is creating a package:
  ``` bash
  $ oe-pkgdata-util lookup-recipe kdump
  kexec-tools
  $ oe-pkgdata-util lookup-recipe libtinfo5
  ncurses
  ```
