# Software update OTA with Yocto: SWUpdate

## Overview

* Update from verified source
* Symmetric encryption
* Web intregrated server
* USB device automatic update
* Compatible Hawkbit
* C API for external programs
* Highly customizable

### Update handlers

SWUpdate provides handlers for a wide range of cases:

* UBI
* Raw flash (NAND, NOR, SPI-NOR, CFI)
* Raw devices copy such as SD card
* Bootloaders config (U-Boot, GRUB, EFI)
* Archive extraction
* Microcontroller flash through UART
* Partitioning tools

And also enables:

* Lua and shell scripts
* Custom handlers

### Sources

* SWUpdate is available under the GPLv2 license
  * <https://github.com/sbabic/swupdate>
* Yocto `meta-swupdate` under the MIT license.
  * <https://github.com/sbabic/meta-swupdate>
* Links to the documentation:
  * <https://sbabic.github.io/swupdate>
  * <https://sbabic.github.io/meta-swupdate>

### SWUpdate command line main usage

``` bash
swupdate
 -f, --file <filename>          : configuration file to use
 -i, --image <filename>         : software to be installed
 -e, --select <software>,<mode> : select software images set and source
 -N, --no-downgrading <version> : not install a release older as <version>
 -R, --no-reinstalling <version>: not install a release same as <version>
 -c, --check                    : check image and exit, use with -i <filename>
 -n, --dry-run                  : run SWUpdate without installing the software
 -M, --no-transaction-marker    : disable setting bootloader transaction marker
 -m, --no-state-marker          : disable setting update state in bootloader
 -k, --key <key>                : public key or certificate for signing verification
 -K, --key-aes <key>            : symmetric key for decryption
```

If no image is specified, `swupdate` will wait. An image can be given to a
running session with the `swupdate-client` command.

Some configuration can be given in a file with the option `-f`[^conffile]

[^conffile]: see
    <https://github.com/sbabic/swupdate/blob/master/examples/configuration/swupdate.cfg>
    for a complete example

## Update description

The update package has the `.swu` extension, it is a cpio archive containing:

* a file `sw-description` that describes what should be installed and where
* a license file
* updates:
  * images
  * archives
  * files
* preinstall and postinstall scripts

### `sw-description`: describe the update

* The format can be `libconfig` or `json` (requires the build option
  `CONFIG_JSON`)
* Any custom Lua parser can be given (e.g. xml[^xmllua]) (build option
  `CONFIG_LUAEXTERNAL`)
* Specify the hardware and software compatibility:
  * a hardware name (*e.g. `beaglebone`*)
  * a hardware revision (*e.g. `rev-1`*)
  * a software selection (*e.g. `syslinbit-project`*)
  * a software mode (used in case of double copy strategy) (*e.g. `faceA`*)
* Tells how and where apply the updates

[^xmllua]: <https://raw.githubusercontent.com/sbabic/swupdate/master/examples/lua-xml/xmlparser.lua>

### sw-description example

``` json
software =
{
	version = "0.1.0";
	description = "Firmware update for XXXXX Project";

	hardware-compatibility: [ "1.0", "1.2", "1.3"];

	images: (
		{
			filename = "sdcard.ext3.gz";
			device = "/dev/mmcblk0p1";
			compressed = "zlib";
		}
	);
}
```

* For every device which hardware revision is 1.0, 1.2 or 1.3, this update will
  copy to `/dev/mmcblk0p1` the raw data contained in the compressed block
  `sdcard.ext3.gz`.

### `sw-description`: boardname differenciation

``` json
software =
{
	version = "0.1.0";
	hardware-compatibility: [ "1.0"];

	hw-1: {
		images: (
			{
				filename = "sdcard.ext3.gz";
				device = "/dev/mmcblk0p1";
				compressed = "zlib";
			}
		);
	}
}
```

* This update will only be applied on hardware `hw-1` of revision `1.0`
* Boardname and hardware revision must be specified to swupdate with the option
  `-H <board>:<revision>`. It can also be store in the file `/etc/hwrevision`

### `sw-description`: software selection

Similarly, we can specify the software:

``` json
software =
{
	version = "0.1.0";

	target: {
		images: (
			{
				filename = "sdcard.ext3.gz";
				device = "/dev/mmcblk0p1";
				compressed = "zlib";
			}
		);
	}
}
```

* The update will only apply to boards running the software `target`
* The software must be specified with the option `-e <software>`.

### `sw-description`: software mode selection

It is possible to increase once the software selection with a mode:

``` json
software = {
	version = "0.1.0";
	target: {
		mode-1: {
			images: ({
					filename = "sdcard.ext3";
					device = "/dev/mmcblk0p1";
		});},
		mode-2: {
			images: ({
					filename = "sdcard.ext3";
					device = "/dev/mmcblk0p2";
});}}}
```

* The update will only apply to boards running the software `target`.
  * If the board is in `mode-1`, `/dev/mmcblk0p1` will be flashed
  * If the board is in `mode-2`, `/dev/mmcblk0p2` will be flashed
* The software must be specified with the option `-e <software>,<mode>`.

### `sw-description`: selection mix and priorities

It is possible to mix hardware and software selection. SWUpdate will search for
an entry according to the following priorities:

1. Try `<boardname>.<selection>.<mode>.<entry>`
1. Try `<selection>.<mode>.<entry>`
1. Try `<boardname>.<entry>`
1. Try `<entry>` (`images` / `files` / `scripts` / `bootenv`)

### `sw-description`: update raw device

Flash a raw image on a block device

``` json
images: (
        {
                filename = <Name in CPIO Archive>;
                device = <destination raw device>;
                mtdname[optional] = <destination mtd name>;
                offset[optional] = <offset>; /* the image can be copied at a specific offset */
                compressed[optional] = <compression mode>; /* the image can be compressed if it is in raw mode */
        }
)
```

* an offset may be specified (in octets (xxx), in kibioctets (xxxK) or mibioctets (xxxM))
* the data can be compressed. If so the variable must be set to `zlib` or `zstd`
  (which needs the build option `CONFIG_ZSTD`).

### `sw-description`: update UBI volume

``` json
images: (
        {
                filename = <Name in CPIO Archive>;
                volume = <destination voume>;
        }
)
```

### `sw-description`: update raw flash

``` json
images: (
        {
                filename = <Name in CPIO Archive>;
                device[optional] = <destination mtd name>;
                mtdname[optional] = <destination mtd name>;
                type = flash;
                offset[optional] = <offset>; /* the image can be copied at a specific offset */
                compressed[optional]; /* the image can be compressed if it is in raw mode */
        }
)
```

* Either `device` (*e.g. `mtdX`*) or `mtdname` (*e.g. `rootfs`*) can be
  specified.

### `sw-description`: the bootloader environment

Either erase the environment:

``` json
images: (
        {
                filename = <Name in the CPIO Archive>;
                type = "bootloader";
        }
)

```

Or modify a variable in the bootloader environment:

``` json
bootenv: (
        {
                name = <Variable name>;
                value = <Variable value>;
        }
)
```

* Both operations are atomic and power-cut safe.

### `sw-description`: install files or extract archives

Install a file or an archive in the rootfs or an unmounted device.

``` json
files: (
        {
                filename = <Name in CPIO Archive>;
                path = <path in filesystem>;
				type[optional] = <type>;
                device[optional] = <device node >;
                filesystem[optional] = <filesystem for mount>;
                properties[optional] = {create-destination = "true";}
                compressed[optional] = <compresion mode>;
        }
);
```

* If `device` is specified, SWUpdate will mount the device before copying the
  file (and then `filesystem` is required).
* `type` can be `archive`

### `sw-description`: scripts

``` json
scripts: (
        {
                filename = <Name in CPIO Archive>;
                type = "lua | shellscript | preinstall | postinstall";
        }
);
```

* `lua` scripts define functions `preinst` and/or `postinst` which will be
  called before and after the install respectively.
* `shellscript` are called via system command before and after the install.
* `preinstall` and `postinstall` are shellscripts that will be called before and
  after the install respectively.

### `sw-description`: versioning

It is possible to manage software versions in swupdate. This requires a file
`/etc/sw-versions` contaning pairs `<name of component> <version>`, such as:

```
bootloader              2015.01-rc3-00456-gd4978d
kernel                  3.17.0-00215-g2e876af
```

This is controlled in `sw-description` by the optional attributes `name`,
`version` and `install-if-different` or `install-if-higher`.

``` json
images: (
        {
                filename = <Name in the CPIO Archive>;
                type = "bootloader";
				name = "bootloader";
				version = "2017.02";
				install-if-higher = true;
        }
)
```

### Sequence

1. extracts and parse `sw-description`
1. verifies `sw-description`, if signing is set
1. checks for hardware compatibility
1. parses `sw-description` to determine the required artifacts.
1. runs embedded scripts, if any. Run pre update command if set.
1. runs partition handlers, if required
1. for each artifact read through the cpio archive:
   1. executes and check handlers for file marked as `installed-directly`
   1. copies and check others files to a temporary location
1. iterates through `scripts` to run pre-install commands
1. iterates through `images` and then through `files`
1. iterates through `scripts` to run post-install commands
1. iterates through `bootenv` and update the bootloader environment
1. reports the status to the operator
1. runs the post update command, if set

## Swupdate tools

### Tools

`swupdate-client`
: send an image to a running instance of SWUpdate and ask it to perform a
  postinstall operation in case of update success.

`swupdate-progress`
: give a progress bar of a running operation


### Mongoose daemon mode

* start as a daemon
* run an integrated web server

![Mongoose web interface](slides/yocto-swupdate/swupdate-mongoose.png){ height=50% }

### Mongoose daemon mode: usage

Requires the build option `CONFIG_WEBSERVER` and `CONFIG_MONGOOSE`.

``` bash
swupdate [OPTIONS] -w '[WS_OPTIONS]'
	  -p, --port <port>              : server port number  (default: 8080)
	  -s, --ssl                      : enable ssl support
	  -C, --ssl-cert <cert>          : ssl certificate to present to clients
	  -K, --ssl-key <key>            : key corresponding to the ssl certificate
	  -r, --document-root <path>     : path to document root directory (default: .)
	  -t, --timeout                  : timeout to check if connection is lost (default: check disabled)
	  --auth-domain                  : set authentication domain if any (default: none)
	  --global-auth-file             : set authentication file if any (default: none)
```

### Suricatta daemon mode

* start as a daemon
* regularly polls a remote server (such as Hawkbit)
* download and install updates
* perform reboot
* report update status
* currently only supports Hawkbit and simple HTTP server but an API is given to
  develop any support

### C API for external programs

* A running instance of SWUpdate can be managed by an external program through
  its API.
* It may download an image if `CONFIG_DOWNLOAD` is enabled in the build
  configuration
* `swupdate-client` and the integrated web server use this API.
* See the documentation[^1] for more informations.

[^1]: <http://sbabic.github.io/swupdate/swupdate-ipc-interface.html>

## U-boot integration

* needs `/etc/fw_env.config` for the bootloader environment description
  (location, size...).
* the bootloader handler must be enabled in the build configuration
* Two u-boot variables are used by design:
  * `recovery_status`
    * set to `in_progress` at the beginning of the update
    * unset when the installation succeeds
    * set to `failed` if not
  * `ustate` can be activated in the build configuration
    * not set or set to `0` when no update is pending
    * set to `1` after an update
    * set to `3` if the update failed

### U-boot: `/etc/fw_env.config` example

``` bash
# NOR example
# MTD device name	Device offset	Env. size	Flash sector size	Number of sectors
/dev/mtd1		0x0000		0x4000		0x4000
/dev/mtd2		0x0000		0x4000		0x4000

# MTD SPI-dataflash example
# MTD device name	Device offset	Env. size	Flash sector size	Number of sectors
#/dev/mtd5		0x4200		0x4200
#/dev/mtd6		0x4200		0x4200

# NAND example
#/dev/mtd0		0x4000		0x4000		0x20000			2

# VFAT example
#/boot/uboot.env	0x0000          0x4000
```

* Find the complete example in the u-boot sources[^ubootsources].
* Values are defined in the u-boot build configuration (`CONFIG_ENV_*` variables
  in `menuconfig`)

[^ubootsources]: <https://github.com/ARM-software/u-boot/blob/master/tools/env/fw_env.config>

## Build integration

### SWUpdate

Configure from bitbake using menuconfig:

``` bash
bitbake swupdate -c menuconfig
```

* SWUpdate Settings: general settings including systemd mode
* Some bootloader options
* Image signing and verification
* Suricatta and Mongoose selection
* Parser selection
* Handlers selection

To make it permanent, replace the `defconfig` file by appending the `swupdate`
recipe.

### SWUpdate daemon configuration

* The daemon is ran through an init script located at `/usr/lib/swupdate/swupdate.sh`.
* It will look for configuration files in `/etc/swupdate/conf.d/` and
  `/usr/lib/swupdate/conf.d/` directories and read them in the alphabetical order.
* Variables that can be modified are:
  * `SWUPDATE_ARGS`[^swuargs]
  * `SWUPDATE_WEBSERVER_ARGS`
  * `SWUPDATE_SURICATTA_ARGS`

[^swuargs]: by default, `SWUPDATE_ARGS="-v ${SWUPDATE_EXTRA_ARGS}"`

### The `swupdate` class

Build an update image

* `SWUPDATE_IMAGES`: list of artifacts to be packaged
* `SWUPDATE_IMAGES_FSTYPES[<image>]`: extension of the artifact
* `SWUPDATE_IMAGES_NOAPPEND_MACHINE[<image>]`: set to `1` to drop the machine
  from the artifact's name
* `SWUPDATE_IMAGES_ENCRYPTED[<image>]`: set to `1` to encrypt the image
* `SWUPDATE_SIGNING`: set to sign the image (`RSA`, `CMS` or `CUSTOM`)
* `SWUPDATE_SIGN_TOOL`: set to use the signing tool (by default `openssl`)
* `SWUPDATE_PRIVATE_KEY`: private key if signing is `RSA`
* `SWUPDATE_PASSWORD_FILE`: optional key password if signing is `RSA`
* `SWUPDATE_CMS_KEY`: private key if signing is `CMS`
* `SWUPDATE_CMS_CERT`: certificate if signing is `CMS`
* `SWUPDATE_AES_FILE`: AES password for symmetric encryption

### SWU image recipe example

``` bash
DESCRIPTION = "Example recipe generating SWU image"
SECTION = ""
LICENSE = "MIT"

# Add all local files to be added to the SWU sw-description must always be in the list.
# You can extend with scripts or wahtever you need
SRC_URI = "file://sw-description"

# images to build before building swupdate image
IMAGE_DEPENDS = "core-image-full-cmdline virtual/kernel"

# images and files that will be included in the .swu image
SWUPDATE_IMAGES = "core-image-full-cmdline uImage"

# a deployable image can have multiple format, choose one
SWUPDATE_IMAGES_FSTYPES[core-image-full-cmdline] = ".ubifs"
SWUPDATE_IMAGES_FSTYPES[uImage] = ".bin"

inherit swupdate
```

## Secure update

* To avoid attacks on the update system, we need to:
  * verify the sources
  * verify the integrity

* Swupdate strategy: sign `sw-description` and give the components checksums
* It is also possible to symmetrically encrypt components

### Signing

* `swupdate`:
  * Build option `CONFIG_SIGNING`
  * Two possible mechanisms:
	* RSA public/private key (config `SIGALG_RAWRSA` or `SIGALG_RSAPSS`)
	* CMS certificates (config `SIGALG_CMS`)
  * Three possible libraries to embed on the board
	* OpenSLL (`CONFIG_SSL_IMPL_OPENSSL`)
	* WolfSSL (`CONFIG_SSL_IMPL_WOLFSSL`)
	* MbedTLS (`CONFIG_SSL_IMPL_MBEDTLS`)
  * Use `-k` to pass the public key or the certificate[^signing].
* In the yocto swupdate image's recipe:
  * set `SWUPDATE_SIGNING` to `RSA`, `CMS` or `CUSTOM`
  * optionally set `SWUPDATE_SIGN_TOOL`
  * set `SWUPDATE_PRIVATE_KEY` and `SWUPDATE_PASSWORD_FILE`

[^signing]: see
    <http://sbabic.github.io/swupdate/signed_images.html#tool-to-generate-keys-certificates>
    for generation

### Checksum

* `swupdate`
  * Build option `CONFIG_HASH_VERIFY` (set by default when signing is activated)
* `sw-description`
  * add a field `sha256`

  ``` json
  software =
  {
      version = "0.1.0";
      hardware-compatibility: [ "revC"];

      images: (
          {
              filename = "core-image-full-cmdline-beaglebone.ext3";
              device = "/dev/mmcblk0p2";
              type = "raw";
              sha256 = "43cdedde429d1ee379a7d91e3e7c4b0b9ff952543a91a55bb2221e5c72cb342b";
          }
      );
  }
  ```

### Encryption 1/2

* `swupdate:`
* Build option `CONFIG_ENCRYPTED_IMAGES`
    * Optionally encrypt `sw-description` with `CONFIG_ENCRYPTED_SW_DESCRIPTION`
  * Use `-K` to pass the AES key file[^aeskey]
  * In `sw-description`, add a field `encrypted=true`
  * `ivt` field can be used to override the initialization vector (`IV`)
  ``` json
  software =
  {
      version = "0.0.1";
      images: (
  	    {
              filename = "core-image-full-cmdline-beaglebone.ext3.enc";
              device = "/dev/mmcblk0p3";
              encrypted = true;
              ivt = "65D793B87B6724BB27954C7664F15FF3";
          }
      );
  }
  ```

[^aeskey]: see <http://sbabic.github.io/swupdate/encrypted_images.html> for AES
    key generation

### Encryption 2/2

* define `SWUPDATE_IMAGES_ENCRYPTED[<image>] = "1"`
* define `SWUPDATE_AES_FILE = "/path/to/aes_key"`[^aesmeta]
  * format is not identical to the one given to `swupdate`

[^aesmeta]: generate with `openssl enc -aes-256-cbc -k <PASSPHRASE> -P -md sha1 > $SWUPDATE_AES_FILE`

### Improved encryption

* change `IV` at each encryption
* in case of several artifacts, encrypt each artifact with a different `IV`
  * in the artifact's recipe:
    * inherit from `swupdate-enc`
	* define `SWUPDATE_AES_FILE = "/path/to/aes_key"`[^aesmeta]
	* add `IMAGE_FSTYPES += "<type>.enc"`
  * in `sw-description` and the yocto swupdate image:
    * add `.enc` to the targeted artifact
	* set the `ivt`

## `sw-description` customization in Yocto

* Automatic sha256 `@<artifact-file-name>`
* Automatic version
  * `@SWU_AUTO_VERSION` set version to `PV`
  * `@SWU_AUTO_VERSION@<package-data-variable>` replace `PV` by an other package variable
  * `@SWU_AUTO_VERSION:<package-name>` to specify the package name if filename
    is different
* BitBake variable expansion `@@VARIABLE@@`

## Good practises

* Prefer Lua scripts to shell scripts
* Use `install-directly` when possible
* Do not drop atomicity
* Forbid downgrades
* Do not leave the private keys in any repository
* Secure the boot process

## Practical lab - manage updates with swupdate

* make an update on the board
* make an update Over The Air
* manage a dual copy with fallback system

