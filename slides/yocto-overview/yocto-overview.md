# Yocto project and poky system overview

## The Yocto project overview

### About

* The Yocto Project is a set of templates, tools and methods that allow to build
  custom embedded Linux-based systems.
* It is an open source project initiated by the Linux Foundation in 2010 and is
  still managed by one of its fellows: Richard Purdie.

### Yocto: principle

![Yocto project principle](slides/yocto-overview/yocto-principle.pdf){
height=60% }

* Yocto always builds binary packages (a "*distribution*")
* The final root filesystem is generated from the package feed
* The [big picture](https://docs.yoctoproject.org/_images/YP-flow-diagram.png)
  is way more complex

### The yocto project lexicon

* The core components of the Yocto Project are:
  * BitBake, the build engine. It is a task scheduler, like make. It interprets
	configuration files and recipes (also called metadata) to perform a set of
	tasks, to download, configure and build specified applications and
	filesystem images.
  * OpenEmbedded-Core, a set of base layers. It is a set of recipes, layers and
	classes which are shared between all OpenEmbedded based systems.
  * Poky, the reference system. It is a collection of projects and tools, used
	to bootstrap a new distribution based on the Yocto Project

### Yocto project overview

![Yocto project overview](slides/yocto-overview/yocto-project-overview.pdf){
height=60% }

### Example of a Yocto Project based BSP

* To build images for a BeagleBone Black, we need:
  * The Poky reference system, containing all common recipes and tools.
  * The `meta-ti-bsp` layer, a set of Texas Instruments specific recipes.
* All modifications are made in the `meta-ti-bsp` layer. Editing Poky or `meta-ti`is a
  **no-go**!
* We will set up this environment in the lab.

## The Poky reference system

### Getting the Poky reference system

* All official projects part of the Yocto Project are available at
  https://git.yoctoproject.org/
* To download the Poky reference system: git clone -b kirkstone
  https://git.yoctoproject.org/git/poky
* Each release has a codename such as **kirkstone** or **honister**, corresponding to a
  release number.
  * A summary can be found at https://wiki.yoctoproject.org/wiki/Releases
* A new version is released every 6 months, and maintained for 7 months
* LTS versions are maintained for 2 years, and announced before their release.

### Poky

![Poky overview](slides/yocto-overview/yocto-overview-poky.pdf){ height=60% }

### Poky source tree 1/2

bitbake/
: Holds all scripts used by the BitBake command. Usually matches the stable
release of the BitBake project.

documentation/
: All documentation sources for the Yocto Project documentation. Can be used to
generate nice PDFs.

meta/
: Contains the OpenEmbedded-Core metadata.

meta-skeleton/
: Contains template recipes for BSP and kernel development.

### Poky source tree 2/2

meta-poky
: Holds the configuration for the Poky reference distribution.

meta-yocto-bsp
: Configuration for the Yocto Project reference hardware board
support package.

LICENSE
: The license under which Poky is distributed (a mix of GPLv2 and MIT).

oe-init-build-env
: Script to set up the OpenEmbedded build environment. It will create the build
directory. It takes an optional parameter which is the build directory name. By
default, this is build. This script has to be sourced because it changes
environment variables.

scripts/
: Contains scripts used to set up the environment, development tools, and tools
to flash the generated images on the target.

### Documentation

* Documentation for the current sources, compiled as a ”mega manual”, is
available at: <https://docs.yoctoproject.org/singleindex.html>

* Variables in particular are described in the variable glossary:
<https://docs.yoctoproject.org/genindex.html>
