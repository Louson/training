# Writing recipes - basics

## Recipes: overview

### Recipes

![Recipe basics
overview](slides/yocto-recipe-basics/yocto-recipe-basics-overview.pdf){ height=80% }

### Basics

* Recipes describe how to handle a given application.
* A recipe is a set of instructions to describe how to retrieve, patch, compile,
  install and generate binary packages for a given application.
* It also defines what build or runtime dependencies are required.
* The recipes are parsed by the BitBake build engine.
* The format of a recipe file name is `<application-name>_<version>.bb`

### Content of a recipe

* A recipe contains configuration variables: name, license, dependencies, path
  to retrieve the source code...
* It also contains functions that can be run (fetch, configure, compile...)
  which are called **tasks**.
* Tasks provide a set of actions to perform.
* Remember the `bitbake -c <task> <target>` command ?

### Common variables

* To make it easier to write a recipe, some variables are automatically
  available:

  `PN`
  : package name, as specified in the recipe file name

  `BPN`
  : `PN`  with prefixes and suffixes removed such as `nativesdk-`, or `-native`

  `PV`
  : package version, as specified in the recipe file name

  `PR`
  : package revision, defaults to `r0`

  `BP`
  : defined as `${BPN}-${PV}`

* The recipe name and version usually match the upstream ones.
* When using the recipe `bash_4.2.bb`:
  * `${PN} = "bash"`
  * `${PV} = "4.2"`

## Organization of a recipe

### Recipe overview

![Recipe basics
organization](slides/yocto-recipe-basics/yocto-recipe-basics-organisation.pdf){ height=80% }

### Recipe organization

* Many applications have more than one recipe, to support different versions. In
that case the common metadata is included in each version specific recipe and is
in a `.inc` file:
  * `<application>.inc`: version agnostic metadata.
  * `<application>_<version>.bb`: `require <application>.inc` and version specific
  metadata.
* We can divide a recipe into three main parts:
  * The header: what/who
  * The sources: where
  * The tasks: how


### The header

Configuration variables to describe the application:

`DESCRIPTION`
: describes what the software is about

`HOMEPAGE`
: URL to the project’s homepage

`PRIORITY`
: defaults to optional

`SECTION`
: package category (e.g. console/utils)

`LICENSE`
: the application’s license

### The source location: overview

* We need to retrieve both the raw sources from an official location and the
  resources needed to configure, patch or install the application.
* `SRC_URI` defines where and how to retrieve the needed elements. It is a set of
  URI schemes pointing to the resource locations (local or remote).
* URI scheme syntax: `scheme://url;param1;param2`
* scheme can describe a local file using `file://` or remote locations with
  `https://`, `git://`, `svn://`, `hg://`, `ftp://`...
* By default, sources are fetched in `$BUILDDIR/downloads`. Change it with the
  `DL_DIR` variable in `conf/local.conf`

### The source locations: remote files 1/2

* The git scheme:
  * `git://<url>;protocol=<protocol>;branch=<branch>`
  * When using git, it is necessary to also define `SRCREV`. If `SRCREV` is a
    hash or a tag not present in master, the branch parameter is mandatory. When
    the tag is not in any branch, it is possible to use `nobranch=1`
* The `http`, `https` and `ftp` schemes:
  * `https://example.com/application-1.0.tar.bz2`
  * A few variables are available to help pointing to remote locations:
    `${SOURCEFORGE_MIRROR}`, `${GNU_MIRROR}`, `${KERNELORG_MIRROR}`...
  * Example: `${SOURCEFORGE_MIRROR}/<project-name>/${BPN}-${PV}.tar.gz`
  * `See meta/conf/bitbake.conf`

### The source locations: remote files 2/2

* An md5 or an sha256 sum must be provided when the protocol used to retrieve the
  file(s) does not guarantee their integrity. This is the case for `https`, `http` or
  `ftp`.
  ```bash
  SRC_URI[md5sum] = "97b2c3fb082241ab5c56ab728522622b"
  SRC_URI[sha256sum] = "..."
  ```
* It’s possible to use checksums for more than one file, using the name
  parameter:
  ```bash
  SRC_URI = "http://example.com/src.tar.bz2;name=tarball \
  http://example.com/fixes.patch;name=patch"
  SRC_URI[tarball.md5sum] = "97b2c3fb082241ab5c56..."
  SRC_URI[patch.md5sum] = "b184acf9eb39df794ffd..."
  ```

### The source locations: local files

* All local files found in `SRC_URI` are copied into the recipe’s working
  directory, in `$BUILDDIR/tmp/work/`.
* The searched paths are defined in the `FILESPATH` variable.
  ```bash
  FILESPATH = "${@base_set_filespath(["${FILE_DIRNAME}/${BP}",
  "${FILE_DIRNAME}/${BPN}","${FILE_DIRNAME}/files"], d)}
  FILESOVERRIDES = "${TRANSLATED_TARGET_ARCH}:
  ${MACHINEOVERRIDES}:${DISTROOVERRIDES}"
  ```
* The `base_set_filespath(path)` function uses its path parameter,
  `FILESEXTRAPATHS` and `FILESOVERRIDES` to fill the `FILESPATH` variable.
* Custom paths and files can be added using `FILESEXTRAPATHS` and
  `FILESOVERRIDES`.
* Prepend the paths, as the order matters.

### The source locations: tarballs

* When extracting a tarball, BitBake expects to find the extracted files in a
  directory named `<application>-<version>`. This is controlled by the `S` variable.
  If the directory has another name, you must explicitly define `S`.
* If the scheme is git, `S` must be set to `${WORKDIR}/git`

### The source locations: license files

* License files must have their own checksum.
* `LIC_FILES_CHKSUM` defines the URI pointing to the license file in the source code
  as well as its checksum.
  ```bash
  LIC_FILES_CHKSUM = "file://gpl.txt;md5=393a5ca..."
  LIC_FILES_CHKSUM = \
  "file://main.c;beginline=3;endline=21;md5=58e..."
  LIC_FILES_CHKSUM = \
  "file://${COMMON_LICENSE_DIR}/MIT;md5=083..."
  ```
* This allows to track any license update: if the license changes, the build
  will trigger a failure as the checksum won’t be valid anymore.

### Dependencies 1/2

* A recipe can have dependencies during the build or at runtime. To reflect these
requirements in the recipe, two variables are used:

  `DEPENDS`
  : List of the recipe build-time dependencies.

  `RDEPENDS`
  : List of the package runtime dependencies. Must be package specific
    (e.g. with `:${PN}`).

* `DEPENDS = "recipe-b"`: the local `do_configure` task depends on the
  `do_populate_sysroot` task of `recipe-b`.
* `RDEPENDS:${PN} = "recipe-b"`: the local `do_build` task depends on the
  `do_package_write_<archive-format>` task of `recipe-b`.

### Dependencies 1/2

* Sometimes a recipe have dependencies on specific versions of another recipe.
* BitBake allows to reflect this by using:
  * `DEPENDS = "recipe-b (>= 1.2)"`
  * `RDEPENDS:${PN} = "recipe-b (>= 1.2)`"
* The following operators are supported: `=`, `>`, `<`, `>=` and `<=`.
* A graphical tool can be used to explore dependencies or reverse dependencies:  
  `bitbake -g -u taskexp core-image-minimal`

### Tasks

Default tasks already exists, they are defined in classes:

* `do_fetch`
* `do_unpack`
* `do_patch`
* `do_configure`
* `do_compile`
* `do_install`
* `do_package`
* `do_rootfs`

You can get a list of existing tasks for a recipe with:  
`bitbake <recipe> -c listtasks`

### Writing tasks 1/2

* Functions use the sh shell syntax, with available OpenEmbedded variables and internal functions available.

  `WORKDIR`
  : the recipe’s working directory

  `S`
  : The directory where the source code is extracted

  `B`
  : The directory where bitbake places the objects generated during the build

  `D`
  : The destination directory (root directory of where the files are installed, before creating the image).

* Syntax of a task:
  ```bash
  do_task() {
      action0
      action1
      ...
  }
  ```

### Writing tasks 2/2

Example:
```bash
do_compile() {
    oe_runmake
}

do_install() {
    install -d ${D}${bindir}
    install -m 0755 hello ${D}${bindir}
}
```

### The main tasks

![Main tasks](slides/yocto-recipe-basics/tasks-basics.pdf){ height=80% }


### Modifying existing tasks

Tasks can be extended with `:prepend` or `:append`:
```bash
do_install:append() {
    install -d ${D}${sysconfdir}
    install -m 0644 hello.conf ${D}${sysconfdir}
}
```

### Adding new tasks

Tasks can be added with addtask:
```bash
do_mkimage () {
    uboot-mkimage ...
}

addtask do_mkimage after do_compile before do_install
```

## Applying patches

### Patches use cases

Patches can be applied to resolve build-system problematics:

* To support old versions of a software: bug and security fixes.
* To fix cross-compilation issues.
* In certain simple cases the -e option of make can be used.
* The `-e` option gives variables taken from the environment precedence over
  variables from Makefiles.
* Helps when an upstream Makefile uses hardcoded `CC` and/or `CFLAGS`.
* To apply patches before they get their way into the upstream version.

### The source locations: patches

* Files ending in `.patch`, `.diff` or having the `apply=yes` parameter will be applied
  after the sources are retrieved and extracted, during the `do_patch` task.
  ```
  SRC_URI += "file://joystick-support.patch \
              file://smp-fixes.diff \
             "
  ```
* Patches are applied in the order they are listed in `SRC_URI`.
* It is possible to select which tool will be used to apply the patches listed
  in `SRC_URI` variable with `PATCHTOOL`.
* By default, `PATCHTOOL = 'quilt'` in Poky.
* Possible values: `git`, `patch` and `quilt`

### Resolving conflicts

* The ```PATCHRESOLVE``` variable defines how to handle conflicts when applying patches.
* It has two valid values:
  * `noop`: the build fails if a patch cannot be successfully applied.
  * `user`: a shell is launched to resolve manually the conflicts.
* By default, `PATCHRESOLVE = "noop"` in meta-poky.


## Example of a recipe

### Hello world recipe

```bash
DESCRIPTION = "Hello world program"
HOMEPAGE = "http://example.net/hello/"
PRIORITY = "optional"
SECTION = "examples"
LICENSE = "GPLv2"
SRC_URI = "git://git.example.com/hello;protocol=https"
SRCREV = "2d47b4eb66e705458a17622c2e09367300a7b118"
S = "${WORKDIR}/git"
LIC_FILES_CHKSUM = \
	"file://hello.c;beginline=3;endline=21;md5=58e..."
```

### Hello world recipe

```bash
do_compile() {
	oe_runmake
}
do_install() {
	install -d ${D}${bindir}
	install -m 0755 hello ${D}${bindir}
}
```

## Example of a recipe with a version agnostic part

### `tar.inc`

```bash
SUMMARY = "GNU file archiving program"
HOMEPAGE = "https://www.gnu.org/software/tar/"
SECTION = "base"

SRC_URI = "${GNU_MIRROR}/tar/tar-${PV}.tar.bz2"

do_configure() { ... }

do_compile() { ... }

do_install() { ... }
```

### `tar_1.17.bb`

```bash
require tar.inc

LICENSE = "GPLv2"
LIC_FILES_CHKSUM = \
	"file://COPYING;md5=59530bdf33659b29e73d4adb9f9f6552"

SRC_URI += "file://avoid_heap_overflow.patch"
SRC_URI[md5sum] = "c6c4f1c075dbf0f75c29737faa58f290"
```

### `tar_1.26.bb`

```bash
require tar.inc

LICENSE = "GPLv3"
LIC_FILES_CHKSUM = \
	"file://COPYING;md5=d32239bcb673463ab874e80d47fae504"

SRC_URI[md5sum] = "2cee42a2ff4f1cd4f9298eeeb2264519"
```

## Practical lab - Add a custom application

* Write a recipe for a custom application
* Integrate it in the image

