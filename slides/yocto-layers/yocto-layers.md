# Layers

## Introduction to layers

### Layers' principles

* The OpenEmbedded *build system* manipulates *metadata*.
* Layers allow to isolate and organize the metadata.
  * A layer is a collection of recipes.
* It is a good practice to begin a layer name with the prefix `meta-`.

### Layers in Poky

![Poky overview](slides/yocto-overview/yocto-overview-poky.pdf){ height=80% }

### Layers in Poky

* The Poky reference system is a set of basic common layers:
  * `meta`
  * `meta-skeleton`
  * `meta-poky`
  * `meta-yocto-bsp`
* Poky is not a final set of layers. It is the common base.
* Layers are added when needed.
* When making modifications to the existing recipes or when adding new ones, it
  is a good practice not to modify Poky. Instead you can create your own layers!

### Third party layers

![Third party layers](slides/yocto-layers/yocto-layer-intro.pdf){ height=80% }

### Integrate and use a layer 1/3

* A list of existing and maintained layers can be found at
  <https://layers.openembedded.org>
* Instead of redeveloping layers, always check the work hasn’t been done by
  others.
* It takes less time to download a layer providing a package you need and to add
  an append file if some modifications are needed than to do it from scratch.

### Integrate and use a layer 2/3

* The location where a layer is saved on the disk doesn’t matter.
* But a good practice is to save it where all others layers are stored.
* The only requirement is to let BitBake know about the new layer:
  * The list of layers BitBake uses is defined in `$BUILDDIR/conf/bblayers.conf`
  * To include a new layer, add its absolute path to the `BBLAYERS` variable.
  * BitBake parses each layer specified in `BBLAYERS` and adds the recipes,
	configurations files and classes it contains.

### Integrate and use a layer 3/3

* The `bitbake-layers` tool is provided alongside `bitbake`.
* It can be used to inspect the layers and to manage  
  `$BUILDDIR/conf/bblayers.conf`:
  * `bitbake-layers show-layers`
  * `bitbake-layers add-layer meta-custom`
  * `bitbake-layers remove-layer meta-qt5`

### Some useful layers

* Many SoC specific layers are available, providing support for the boards using
  these SoCs. Some examples: `meta-ti-bsp`, `meta-freescale` and `meta-raspberrypi`.
* Other layers offer to support applications not available in the Poky reference
  system:
  * `meta-browser`: web browsers (Chromium, Firefox).
  * `meta-filesystems`: support for additional filesystems.
  * `meta-gstreamer10`: support for GStreamer 1.0.
  * `meta-java` and `meta-oracle-java`: Java support.
  * `meta-linaro-toolchain`: Linaro toolchain recipes.
  * `meta-qt5`: QT5 modules.
  * `meta-realtime`: real time tools and test programs.
  * `meta-telephony` and many more...

  Notice that some of these layers do not come with all the Yocto branches. The
  `meta-browser` did not have a `krogoth` branch, for example.

## Creating a layer

![Custom layer](slides/yocto-layers/yocto-layer-create.pdf){ height=80% }

### Create a custom layer 1/2

* A layer is a set of files and directories and can be created by hand.
* However, the `bitbake-layers create-layer` command helps us create new
  layers and ensures this is done right.  
  `bitbake-layers create-layer -p <PRIORITY> <layer>`
  * The priority is used to select which recipe to use when multiple layers
    contains the same recipe
  * The recipe priority takes precedence over the version number ordering

### Create a custom layer 2/2

* The layer created will be pre-filled with the following files:

  `conf/layer.conf`
  : The layer’s configuration. Holds its priority and generic information. No need to modify it in many cases.

  `COPYING.MIT`
  : The license under which a layer is released. By default MIT.

  `README`
  : A basic description of the layer. Contains a contact e-mail to update.

* By default, all metadata matching `./recipes-*/*/*.bb` will be parsed by the
  BitBake build engine.

### Use a layer: best practices

* Do not copy and modify existing recipes from other layers. Instead use append
  files.
* Avoid duplicating files. Use append files or explicitly use a path relative to
  other layers.
* Save the layer alongside other layers, in `OEROOT`.
* Use `LAYERDEPENDS` to explicitly define layer dependencies.
* Use `LAYERSERIES_COMPAT` to define the Yocto version(s) with which the layer is
  compatible.

### Practical lab - Create a custom layer

* Create a layer from scratch
* Add recipes to the new layer
* Integrate it to the build

### Practical lab - Extend a recipe

* Apply patches to an existing recipe
* Use a custom configuration file for an existing
recipe
* Extend a recipe to fit your needs

