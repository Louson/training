# Using Yocto Project - advanced usage

* Select package variants.
* Manually add packages to the generated image.
* Run specific tasks with BitBake.

### A little reminder

* Recipes describe how to fetch, configure, compile and install applications.
* These tasks can be run independently (if their dependencies are met).
* All available packages in Poky are not selected by default in the images.
* Some packages may provide the same functionality, e.g. OpenSSH and Dropbear.

## Variables

### Overview

* The OpenEmbedded build system uses configuration variables to hold
  information.
* Variable names are in upper-case by convention, e.g. `CONF_VERSION`
* Vaiable values are strings.
* To make configuration easier, it is possible to prepend, append or define
  these variables in a conditional way.
* Variables defined in **Configuration Files** have a **global** scope
  * Files ending in `.conf`
* Variables defined in **Recipes** have a **local** scope
  * Files ending in `.bb`, `.bbappend` and `.bbclass`
* Recipes can also access the global scope
* All variables can be overridden or modified in `$BUILDDIR/conf/local.conf`

### Operators

Various operators can be used to assign values to configuration variables:

`=`
: expand the value when using the variable

`:=`
: immediately expand the value

`+=`
: append (with space)

`=+`
: prepend (with space)

`.=`
: append (without space)

`=.`
: prepend (without space)

`?=`
: assign if no other value was previously assigned

`??=`
: same as previous, with a lower precedence

### Operators caveats

* The operators apply their effect during parsing
* The parsing order is difficult to predict, no assumption should be made about
  it
  * Example: if `+=` is parsed before `?=`, the latter will be discarded.
* To avoid problem, avoid using `+=`, `=+`, `.=` and `=.` in
  `$BUILDDIR/conf/local.conf`. Always use overrides (see following slides).

### Overrides

* Bitbake **overrides** allow appending, prepending or modifying a variable at
  expansion tome, when the variable's value is read
* Overrides are written as `<VARIABLE>:<override> = "some_value"`
* A different syntax was used before **Honister**, with no retrocompatibility:
  `<VARIABLE>_<override> = "some_value"`

### Overrides to modify variable values

* The `append` override adds **at the end** of the variable (without space).
  * `IMAGE_INSTALL:append = " dropbear"` adds dropbear to the packages installed
	on the image.
* The `prepend` override adds **at the beginning** of the variable (without space).
  * `FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}: "` adds the folder to the set
	of paths where files are located (in a recipe).
* The `remove` override remove all occurences of a value within a variable.
  * `IMAGE_INSTALL:remove = "i2c-tools"`

### Overrides for a conditional assignment

* Append the machine name to only define a configuration variable for a given
  machine. It tries to match with values from `OVERRIDES` which includes
  `MACHINE`, `SOC_FAMILY`

```bash
OVERRIDES="arm:armv7a:ti-soc:ti33x:beaglebone:poky"
KERNEL_DEVICETREE:beaglebone = "am335x-bone.dtb" # This is applied
KERNEL_DEVICETREE:dra7xx-evm = "dra7-evm.dtb"    # This is ignored
```

### Combining overrides 1/2

* The previous methods can be combined.
* If we define:
  ```
  IMAGE_INSTALL = "busybox mtd-utils"
  IMAGE_INSTALL:append = " dropbear"
  IMAGE_INSTALL:append:beaglebone = " i2c-tools"
  ```

* The resulting configuration variable will be:
  * `IMAGE_INSTALL = "busybox mtd-utils dropbear i2c-tools"` if the machine
    being built is `beaglebone`.
  * `IMAGE_INSTALL = "busybox mtd-utils dropbear"` otherwise.

### Combining overrides 2/2

* The most specific variable takes precedence.
* Example:
  ```
  IMAGE_INSTALL:beaglebone = "busybox mtd-utils i2c-tools"
  IMAGE_INSTALL = "busybox mtd-utils"
  ```
  * If the machine is `beaglebone`:  
	`IMAGE_INSTALL = "busybox mtd-utils i2c-tools"`
  * Otherwise:  
	`IMAGE_INSTALL = "busybox mtd-utils"`

### Order of variable assignment

![Operators order](slides/yocto-advanced/yocto-operators-order.pdf){ height=50% }

* All the operators are applied, in parsing order
* `:append` overrides are applied
* `:prepend` overrides are applied
* `:remove` overrides are applied

## Packages variants

### Introduction to package variants

* Some packages have the same purpose, and only one can be used at a time.
* The build system uses virtual packages to reflect this. A virtual package
  describes functionalities and several packages may provide it.
* Only one of the packages that provide the functionality will be compiled and
  integrated into the resulting image.

### Variant examples

* The virtual packages are often in the form `virtual/<name>`
* Example of available virtual packages with some of their variants:
  * `virtual/bootloader`: u-boot, u-boot-ti-staging...
  * `virtual/kernel`: linux-yocto, linux-yocto-tiny, linux-yocto-rt,
	linux-ti-staging...
  * `virtual/libc`: glibc, musl, newlib
  * `virtual/xserver`: xserver-xorg

### Package selection

* Variants are selected thanks to the `PREFERRED_PROVIDER` configuration variable.
* The package names **have to** suffix this variable.
* Examples:
  * `PREFERRED_PROVIDER_virtual/kernel ?= "linux-ti-staging"`
  * `PREFERRED_PROVIDER_virtual/libgl = "mesa"`


### Version selection

* By default, Bitbake will try to build the provider with the highest version
  number, from the highest priority layer, unless the recipe defines  
  `DEFAULT_PREFERENCE = "-1"`
* When multiple package versions are available, it is also possible to
  explicitly pick a given version with `PREFERRED_VERSION`.
* The package names **have to** suffix this variable.
* `%` can be used as a wildcard.
* Example:
  * `PREFERRED_VERSION_nginx = "1.20.1"`
  * `PREFERRED_VERSION_linux-yocto = "5.14%"`

## Packages

### Selection

* The set of packages installed into the image is defined by the target you
  choose (e.g. `core-image-minimal`).
* It is possible to have a custom set by defining our own target, and we will
  see this later.
* When developing or debugging, adding packages can be useful, without modifying
  the recipes.
* Packages are controlled by the `IMAGE_INSTALL` configuration variable.

### Exclusion

* The list of packages to install is also filtered using the `PACKAGE_EXCLUDE`
  variable.
* If you choose to not install a package using this variable and some other
  package is dependent on it (i.e. listed in a recipe’s `RDEPENDS` variable),
  the OpenEmbedded build system generates a fatal installation error.
* This only works with RPM and IPK packages.

## The power of BitBake

### Common BitBake options

* BitBake can be used to run a full build for a given target with `bitbake
  [target]`
	  * `target` is a recipe name, possibly wih modifiers, e.g. `-native`
		  * `bitbake ncurses`
		  * `bitbake ncurses-native`
* But it can be more precise, with optional options:

  `-c <task>`
  : execute the given `task`

  `-s`
  : list all locally available receipes and their versions

  `-f`
  : force the given task to be run by removing its stamp file world keyword
	for all recipes

  `-b <recipe>`
  : execute tasks from the given `recipe` (without resolving dependencies).

### BitBake examples

* `bitbake -c listtasks virtual/kernel`
  * Gives a list of the available tasks for the recipe providing the package
    `virtual/kernel`. Tasks are prefixed with `do_`.
* `bitbake -c menuconfig virtual/kernel`
  * Execute the task `menuconfig` on the recipe providing the `virtual/kernel`
    package.
* `bitbake -f dropbear`
  * Force the dropbear recipe to run all tasks.
* bitbake world `--runall=fetch`
  * Download all recipe sources and their dependencies.
* For a full description: `bitbake --help`

### shared state cache

* BitBake stores the output of each task in a directory, the shared state
  cache. Its location is controlled by the `SSTATE_DIR` variable.
* This cache is use to speed up compilation.
* Over time, as you compile more recipes, it can grow quite big. It is possible
  to clean old data with:
  ```
  $ ./scripts/sstate-cache-management.sh -y -d \
  --cache-dir=$SSTATE_DIR
  ```

### Practical lab - Advanced Yocto configuration

* Modify the build configuration
* Customize the package selection
* Experiment with BitBake
* Mount the root file system over NFS

