### Hyperlinks in the document

There are many hyperlinks in the document

* Regular hyperlinks:  
  <https://kernel.org/>
* Kernel documentation links:  
  [`dev-tools/kasan`](https://www.kernel.org/doc/html/latest/dev-tools/kasan.html)
* Links to kernel source files and directories:  
  [`drivers/input/`](https://elixir.bootlin.com/linux/latest/source/drivers/input/)  
  [`include/linux/fb.h`](https://elixir.bootlin.com/linux/latest/source/include/linux/fb.h)
* Links to the declarations, definitions and instances of kernel symbols (functions,
  types, data, structures):  
  [`platform_get_irq()`](https://elixir.bootlin.com/linux/latest/ident/platform_get_irq)  
  [`GFP_KERNEL`](https://elixir.bootlin.com/linux/latest/ident/GFP_KERNEL)  
  [`struct_file_operations`](https://elixir.bootlin.com/linux/latest/ident/file_operations)
