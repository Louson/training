# Software update OTA (On The Air)

## Introduction

### Strategy 1: callback product || technician on site

| Advantages           | Drawbacks                                              |
| ----------           | ----------                                             |
| Low development cost | The technician must be trained and have specific tools |
|                      | The fix may depend on the expedition duration          |

### Reasons to update OTA

* Enhance security
* Face to unpredicted vulnerabilities
* Avoid product recall
* Avoid user intervention
* Enhance product life cycle
* Keep every board of a fleet at the same status

### Risks of an OTA update system

* Power down during update (power failure, battery down, user switchs off by
  accident)
* Incompatible update
* Procedure failure
* Hardware write failure
* Cyber attack with firmware update

### Strategy 2: package update

| Advantages                         | Drawbacks                                                    |
| ----------                         | ----------                                                   |
| Low impact                         | Hard to know the status of a product                         |
| Continious update                  | Products have various states                                 |
| No danger on non-critical packages | Update not tested for all cases                              |
|                                    | Critical packages update can lead to an unsolvable situation |
|                                    | Requires specific tools to serve the packages                |

### Strategy 3: bootloader update

* The new firmware is installed somewhere on the machine.
* The update is managed by the bootloader.
* It checks the integrity of the image and flash the system.

| Advantages              | Drawbacks                              |
| ----------              | ----------                             |
| Minimal memory required | Requires to patch the bootloader       |
|                         | Limited FS support                     |
|                         | Limited network support                |
|                         | Updating the bootloader is discouraged |
|                         | System unavailable during the update   |
|                         | No rollback possible                   |

### Strategy 4: dedicated partition (recovery mode)

* The update is performed by a recovery on a dedicated system.
* In case of boot failure, the system will reboot in recovery mode, enabling any
  new update.

| Advantages                                        | Drawbacks                            |
| ----------                                        | ----------                           |
| Low memory required                               | The recovery is a cyberattack target |
| On failure, the system will fall in recovery mode | Updating the recovery is discouraged |
| Hard to brick                                     | System unavailable during the update |
|                                                   | No rollback possible                 |

### Strategy 5: double copy with fall-back

* Two copies of the system coexist. The update is performed by the running
  system.
* Next boot, the system will boot on the other partition.
* In case of boot failure, the system will reboot on the previous partition,
  running as before and enabling any new update.

| Advantages                                 | Drawbacks                                                    |
| ----------                                 | ----------                                                   |
| System available during the update         | Large space needed: the data is duplicated                   |
| System remains available on update failure | If there is a hidden issue, both partitions can be corrupted |


### OTA principles: Redundancy & atomicity

#### Redundancy

* Do not update the running system
* Always keep a bootable machine

#### Atomicity

* Only select the system once the update is verified
* The switch to the updated system must be atomic

### Potential targets

![Linux system architecture](slides/yocto-update/linux-system-architecture.pdf){
height=70% }

## Scenario 1/2: Recovery partition

![Update in recovery](slides/yocto-update/update-recovery.pdf){
height=70% }

### Procedure

1. The running system detects an update
1. The running system tells the bootloader to boot on the recovery
1. The system reboots
1. The bootloader boots on the recovery
1. The bootloader downloads the firmware and checks its integrity
1. The bootloader erase the main partition
1. The system reboots
1. The bootloader tries to boot the new firmware
1. The new firmware tells the booloader that the boot is done
1. At next boot, the system will boot normally

## Scenario 2/2: Double copy with fall-back

![Update in double copy with fallback](slides/yocto-update/update-double-copy.pdf){
height=70% }

### Procedure

1. The running system A detects an update
1. It downloads the firmware and checks its integrity
1. It writes the partition B
1. It tells the bootloader an update has been made
1. The system reboots
1. The bootloader tries to boot the new firmware on partition B
1. The new firmware tells the booloader that the boot is done
1. At next boot, the system will boot normally on the partition B

A and B are symmetric and reversable.

## Kernel and device tree

Which strategy for the kernel and the device tree ?

* Single Linux kernel (and device tree) on a dedicated boot partition
  * never updated
* Linux kernel and device tree in the root file system
  * updated with the applications
* Duplicated Linux kernel (and device tree) in two dedicated boot partitions
  * update is independant
  * boot sequence more complex

## Important considerations

* Define clearly when the boot is completed
* Reduce dependencies
* Define what can be updated by evaluting the benefits and the risks
* Think about a watchdog that will reset the board if blocked
* Reset in case of kernel panic (option `panic=X`)
* Think about a rescue system even in dual copy with fallback

## Examples of cyber attacks on system update

* replace the firmware by a corrupted firmware
  * the attacker replaces the firmware by its own
  * solution: check the origin and the integrity of the firmware
* replace the firmware by an old release
  * the attacker replace the firmware by an old one with known weakness
  * solution: forbid downgrades
* downgrade by forcing the boot on the fallback
  * the attacker blocks the boot process and makes it boot on the fallback which
    has a lower version
  * solution: evaluate the risk of having all partitions updated
  * warning: when all partitions are updated, there is no fallback possible
* attack on the recovery
  * the attacker replaces the recovery system which is less updated and weaker
