YOCTO_SLIDES = \
	bootlin-base preamble \
	embeddedlinux \
	yocto-overview yocto-basics yocto-advanced \
	yocto-recipe-basics yocto-recipe-advanced \
	yocto-layers yocto-layer-bsp yocto-layer-distro \
	yocto-images yocto-recipe-extra yocto-licensing \
	yocto-sdk yocto-runtime-package-management yocto-resources \
	yocto-update yocto-swupdate

YOCTO_LABS   = \
	bootlin-base preamble \
	yocto-first-build yocto-advanced-configuration \
	yocto-add-application yocto-layer yocto-extend-recipe \
	yocto-custom-machine yocto-custom-image yocto-sdk \
	yocto-swupdate

YOCTO_LABS_DATA = \
	yocto-first-build/0001-Simplify-linux-ti-staging-5.4-recipe.patch
