---
title: 'Example'
subtitle: 'Example for documentation'
author: Louis Rannou
date: 01/01/2022
lang: en-GB

# Beamer
aspectratio: 169
beamer-option:
institute: syslinbit
logo:
theme: syslinbit
colortheme: beaver
themeoptions:

# Fonts
fontsize: 10pt
---
