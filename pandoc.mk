# Input variables. Either TARGET, CHAPTER or TRAINING must be given
# TARGET: build a whole target slides or labs
# CHAPTER build a specific slides' chapter
# TRAINING: build a specific labs' training

include .config

INKSCAPE = inkscape
DIA      = dia
EPSTOPDF = epstopdf
PANDOC   = pandoc

INKSCAPE_IS_NEW = $(shell inkscape --version 2>/dev/null | grep -q "^Inkscape 1" && echo YES)
ifeq ($(INKSCAPE_IS_NEW),YES)
INKSCAPE_PDF_OPT = -o
else
INKSCAPE_PDF_OPT = -A
endif

PROJECTDIR = $(shell pwd)
OUTDIR     = $(PROJECTDIR)/out

# Needed macros
UPPERCASE = $(shell echo $1 | tr "[:lower:]-" "[:upper:]_")
$(shell mkdir -p $(OUTDIR) && \
	t=`git log -1 --format=%ct` && \
	printf "lastupdate: %s\n" "`(LANG=en_EN.UTF-8 date -d @$${t} +'%B %d, %Y')`" \
		> $(OUTDIR)/last-update.metadata)
# Strip quotes and then whitespaces
qstrip = $(strip $(subst ",,$(1)))
# "))
# Export config qstripped
LABCONFIG = $(call qstrip, $(CONFIG_LAB_$(call UPPERCASE, $(1))))
SLIDECONFIG = $(call qstrip, $(CONFIG_SLIDE_$(call UPPERCASE, $(1))))

ifeq ($(TARGET),)
else
include targets/$(TARGET).mk
endif

# Pandoc opts
PANDOC_TP = common/syslinbitpandoc
PANDOC_METADATAFILES = common/syslinbit.metadata
PANDOC_OPTS = \
	-s --pdf-engine=xelatex \
	--metadata-file=last-update.metadata \
	$(foreach m,$(PANDOC_METADATAFILES),--metadata-file=$(m))

# Pandoc slides opts
SLIDES_TYPE = beamer
PANDOC_SLIDES_OPTS = -t $(SLIDES_TYPE) --highlight-style=tango
PANDOC_BEAMER_METADATA = common/beamer.metadata
PANDOC_BEAMER_THEME = syslinbit
PANDOC_BEAMER_OPTS = --template=$(PANDOC_TP) \
	$(foreach m,$(PANDOC_BEAMER_METADATA),--metadata-file=$(m)) \
	-V theme:$(PANDOC_BEAMER_THEME) \
	--slide-level=3

ifneq ($(INTERACTIVE),)
PANDOC_BEAMER_OPTS := $(PANDOC_BEAMER_OPTS) -i
endif

# Pandoc labs opts
PANDOC_LABS_METADATA = common/labs.metadata
PANDOC_LABS_OPTS = --template=$(PANDOC_TP) \
	$(foreach m,$(PANDOC_LABS_METADATA),--metadata-file=$(m)) \
	--top-level-division=chapter -N \
	--highlight-style=zenburn

# Pandoc commands
PANDOC_SLIDES = $(PANDOC) $(PANDOC_OPTS) $(PANDOC_SLIDES_OPTS) $(PANDOC_BEAMER_OPTS)
PANDOC_LABS = $(PANDOC) $(PANDOC_OPTS) $(PANDOC_LABS_OPTS)

#
# === Common lookup
#
COMMON_FILES = \
	$(foreach f, $(PANDOC_METADATAFILES), $(OUTDIR)/$(f))
SLIDES_COMMON_FILES = \
	$(COMMON_FILES) \
	$(OUTDIR)/$(PANDOC_TP).beamer \
	$(OUTDIR)/$(PANDOC_BEAMER_METADATA) \
	$(OUTDIR)/beamertheme$(PANDOC_BEAMER_THEME).sty
LABS_COMMON_FILES = \
	$(COMMON_FILES) \
	$(OUTDIR)/$(PANDOC_TP).latex \
	$(OUTDIR)/$(PANDOC_LABS_METADATA)

#
# === Picture lookup ===
#

# Function that computes the list of pictures of the extension given
# in $(2) from the directories in $(1), and transforms the filenames
# in .pdf in the output directory. This is used to compute the list of
# .pdf files that need to be generated from .dia or .svg files.
PICTURES_WITH_TRANSFORMATION = \
	$(patsubst %.$(2),$(OUTDIR)/%.pdf,$(foreach s,$(1),$(wildcard $(s)/*.$(2))))

# Function that computes the list of pictures of the extension given
# in $(2) from the directories in $(1). This is used for pictures that
# to not need any transformation, such as bitmap files in the .png or
# .jpg formats.
PICTURES_NO_TRANSFORMATION = \
	$(patsubst %,$(OUTDIR)/%,$(foreach s,$(1),$(wildcard $(s)/*.$(2))))

# Function that computes the list of pictures from the directories in
# $(1) and returns output filenames in the output directory.
PICTURES = \
	$(call PICTURES_WITH_TRANSFORMATION,$(1),svg) \
	$(call PICTURES_WITH_TRANSFORMATION,$(1),dia) \
	$(call PICTURES_NO_TRANSFORMATION,$(1),png)   \
	$(call PICTURES_NO_TRANSFORMATION,$(1),jpg)   \
	$(call PICTURES_NO_TRANSFORMATION,$(1),pdf)

# List of common pictures
COMMON_PICTURES   = $(call PICTURES,common)

#
# === Basic targets ===
#
default: error

%-course: %-slides.pdf %-labs.pdf
	@echo ""

#
# === Compilation of slides ===
#
SLIDES_CHAPTERS = $($(call UPPERCASE, $(basename $(TARGET)))_SLIDES)
SLIDES_COMMON_BEFORE = targets/$(basename $(TARGET)).md
SLIDES_COMMON_AFTER = common/slides_footer.md
SLIDES_MD = \
	$(foreach s,$(SLIDES_COMMON_BEFORE), $(OUTDIR)/$(s)) \
	$(foreach s,$(SLIDES_CHAPTERS),$(OUTDIR)/$(wildcard slides/$(s)/$(s)$(call SLIDECONFIG, $(s)).md)) \
	$(OUTDIR)/$(SLIDES_COMMON_AFTER)
SLIDES_PICTURES = $(call PICTURES,$(foreach s,$(SLIDES_CHAPTERS),slides/$(s))) $(COMMON_PICTURES)

.PRECIOUS: %-slides.tex %-slides.pdf
%-slides.tex: $(SLIDES_COMMON_FILES) $(SLIDES_PICTURES) $(SLIDES_MD)
	(cd $(OUTDIR); $(PANDOC_SLIDES) $(SLIDES_MD) -o $@)
	cat $(OUTDIR)/$@ > $@

%-slides.pdf: $(SLIDES_COMMON_FILES) $(SLIDES_PICTURES) $(SLIDES_MD)
	(cd $(OUTDIR); $(PANDOC_SLIDES) $(SLIDES_MD) -o $@)
	cat $(OUTDIR)/$@ > $@

#
# === Compilation of specific slides' chapter ===
#
SLIDES_CHAPTER_BEFORE = targets/generic.md
SLIDES_CHAPTER_MD = \
	$(foreach s,$(SLIDES_CHAPTER_BEFORE), $(OUTDIR)/$(s)) \
	$(foreach s,$(CHAPTER),$(OUTDIR)/$(wildcard slides/$(s)/$(s)$(call SLIDECONFIG, $(s)).md))
SLIDES_CHAPTER_PICTURES = $(call PICTURES, slides/$(CHAPTER))

%-chapter.pdf: $(SLIDES_COMMON_FILES) $(SLIDES_CHAPTER_PICTURES) $(SLIDES_CHAPTER_MD)
	(cd $(OUTDIR); $(PANDOC_SLIDES) $(SLIDES_CHAPTER_MD) -o $@)
	cat $(OUTDIR)/$@ > $@

%-chapter.tex: $(SLIDES_COMMON_FILES) $(SLIDES_PICTURES) $(SLIDES_CHAPTER_MD)
	(cd $(OUTDIR); $(PANDOC_SLIDES) $(SLIDES_CHAPTER_MD) -o $@)
	cat $(OUTDIR)/$@ > $@

#
# === Compilation of labs ===
#
LABS_TRAININGS = $($(call UPPERCASE, $(basename $(TARGET)))_LABS)
LABS_COMMON_BEFORE = targets/$(basename $(TARGET)).md
LABS_COMMON_AFTER =
LABS_MD = \
	$(foreach s,$(LABS_COMMON_BEFORE),$(OUTDIR)/$(s)) \
	$(foreach s,$(LABS_TRAININGS),$(OUTDIR)/$(wildcard labs/$(s)/$(s)$(call LABCONFIG, $(s)).md)) \
	$(foreach s,$(LABS_COMMON_AFTER),$(OUTDIR)/$(s))
LABS_PICTURES = $(call PICTURES,$(foreach s,$(LABS_TRAININGS),labs/$(s))) $(COMMON_PICTURES)

.PRECIOUS: %-labs.tex %-labs.pdf
%-labs.tex: $(LABS_COMMON_FILES) $(LABS_PICTURES) $(LABS_MD)
	(cd $(OUTDIR); $(PANDOC_LABS) $(LABS_MD) -o $@)
	cat $(OUTDIR)/$@ > $@

%-labs.pdf: $(LABS_COMMON_FILES) $(LABS_PICTURES) $(LABS_MD)
	(cd $(OUTDIR); $(PANDOC_LABS) $(LABS_MD) -o $@)
	cat $(OUTDIR)/$@ > $@

#
# === Compilation of specific labs' training ===
#
LABS_TRAINING_BEFORE = targets/generic.md
LABS_TRAINING_MD = \
	$(foreach s,$(LABS_TRAINING_BEFORE), $(OUTDIR)/$(s)) \
	$(foreach s,$(TRAINING),$(OUTDIR)/$(wildcard labs/$(s)/$(s)$(call LABCONFIG, $(s)).md))

%-lab.pdf: $(LABS_COMMON_FILES) $(LABS_PICTURES) $(LABS_TRAINING_MD)
	(cd $(OUTDIR); $(PANDOC_LABS) $(LABS_TRAINING_MD) -o $@)
	cat $(OUTDIR)/$@ > $@

%-lab.tex: $(LABS_COMMON_FILES) $(LABS_PICTURES) $(LABS_TRAINING_MD)
	(cd $(OUTDIR); $(PANDOC_LABS) $(LABS_TRAINING_MD) -o $@)
	cat $(OUTDIR)/$@ > $@

#
# === Picture generation ===
#
.PRECIOUS: $(OUTDIR)/%.pdf

$(OUTDIR)/%.pdf: %.svg
	@printf "%-15s%-20s->%20s\n" INKSCAPE $(notdir $^) $(notdir $@)
	@mkdir -p $(dir $@)
ifeq ($(V),)
	$(INKSCAPE) -D $(INKSCAPE_PDF_OPT) $@ $< > /dev/null 2>&1
else
	$(INKSCAPE) -D $(INKSCAPE_PDF_OPT) $@ $<
endif

$(OUTDIR)/%.pdf: $(OUTDIR)/%.eps
	@printf "%-15s%-20s->%20s\n" EPSTOPDF $(notdir $^) $(notdir $@)
	@mkdir -p $(dir $@)
	$(EPSTOPDF) --outfile=$@ $^

.PRECIOUS: $(OUTDIR)/%.eps

$(OUTDIR)/%.eps: %.dia
	@printf "%-15s%-20s->%20s\n" DIA $(notdir $^) $(notdir $@)
	@mkdir -p $(dir $@)
	$(DIA) -e $@ -t eps $^

.PRECIOUS: $(OUTDIR)/%.png

$(OUTDIR)/%.png: %.png
	@mkdir -p $(dir $@)
	@cp $^ $@

.PRECIOUS: $(OUTDIR)/%.jpg

$(OUTDIR)/%.jpg: %.jpg
	mkdir -p $(dir $@)
	@cp $^ $@

$(OUTDIR)/%.pdf: %.pdf
	mkdir -p $(dir $@)
	@cp $^ $@

#
# === Common generation ===
#.

#
# === Generic md rule that copies the file in the output directory ===
#
.PRECIOUS: $(OUTDIR)/%.md
$(OUTDIR)/%.md: %.md
	@mkdir -p $(dir $@)
	@cp $^ $@

.PRECIOUS: $(OUTDIR)/%.metadata
$(OUTDIR)/%.metadata: %.metadata
	@mkdir -p $(dir $@)
	@cp $^ $@

.PRECIOUS: $(OUTDIR)/%.beamer $(OUTDIR)/%.latex
$(OUTDIR)/%.beamer: %.beamer
	@mkdir -p $(dir $@)
	@cp $^ $@

$(OUTDIR)/%.latex: %.latex
	@mkdir -p $(dir $@)
	@cp $^ $@

.PRECIOUS: $(OUTDIR)/%.sty
$(OUTDIR)/%.sty: common/%.sty
	@mkdir -p $(OUTDIR)
	@cp $^ $(OUTDIR)/

#
# === Lab data archive generation
#
LABS_DATAFILES = \
	$(foreach s, $($(call UPPERCASE, $(TARGET))_LABS_DATA), labs/$(s))

.PRECIOUS: %-labs.tar.xz

%-labs.tar.xz: OUT_LAB_DATA=$(OUTDIR)/$(TARGET)-labs
%-labs.tar.xz: %-slides.pdf %-labs.pdf $(LABS_DATAFILES)
	$(RM) -rf $(OUT_LAB_DATA)
	$(RM) -f $@
	mkdir -p $(OUT_LAB_DATA)
	cp -r $(LABS_DATAFILES) $(OUT_LAB_DATA)/
	(tar Jcf $@ -C $(OUTDIR) $(TARGET)-labs $(TARGET)-slides.pdf $(TARGET)-labs.pdf)

clean:
	$(RM) -rf $(OUTDIR) *.pdf *.tex *.tar.xz

#
# === Some debug targets
#
targets/%.mk:
	@echo "File '$@' is missing"
	exit 1

targets/%.md:
	@echo "File '$@' is missing"
	exit 1
