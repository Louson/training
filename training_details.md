* - Comprendre les bases de la compilation croisée
  - Comparer yocto/OE avec d'autres chaînes (classiques: toolchain de
la distrib ou téléchargée, toolchain fabriquée ; moderne: buildroot)
* - Configurer et réaliser la compilation, installer le système sur une cible
  - Chercher une recette, un layer
* - Créer ou personnaliser des recettes
  - Créer un layer
  - Créer une image personnalisée
  - Utiliser des package groups
* intégrer le support d'une carte
* lire la documentation
* comprendre les classes, explorer poky
* comprendre bitbake (les tâches, les options)
* Gérer ses layers avec yocto-cooker

* Que faire quand ça ne marche pas

* Comprendre ce que ça implique :
     - des contraintes liées à la layer du constructeur
     - besoin de mises à jour régulières parce que les grosses mises à
jour sont très pénibles

* avancé: mise à jour, configuration mémoire, FS, classes, systemd
* LTS



## Notes

Build system =
* Cross-compiler des sources
* Partir avec des solutions existantes, ne pas réinventer la roue
* mega manual
* swupdate
