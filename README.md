# Syslinbit trainings

## Compile

This slides and documents use [pandoc](https://pandoc.org). It converts markdown
to latex and latex to PDF.

Here is the packages you need to compile:

### Ubuntu

```
pandoc git-core inkscape texlive-latex-base texlive-latex-extra \
texlive-font-utils dia python-pygments python3-pygments \
texlive-fonts-recommended texlive-fonts-extra make texlive-xetex \
texlive-extra-utils fonts-inconsolata fonts-liberation ttf-ubuntu-font-family \
xfonts-scalable lmodern texlive-science texlive-plain-generic
```

### Arch

```
pandoc git inkscape texlive-most texlive-fontsextra dia python-pygments \
ghostscript ttf-inconsolata otf-latin-modern
```

Then, run `make list-courses` to print all available courses or `make help` to
see all possible targets.

## Configure

For some chapters or taining, different choices are available. To configure it,
you need [`python-kconfiglib`](https://pypi.org/project/kconfiglib) or any
similar software that manages a `Kconfig`.

```
MENUCONFIG_STYLE=monochrome menuconfig
```

or

```
guiconfig
```

The configuration will be saved in `.config`.

Some default configuration are available in the `configs` repertory. Run
```defconfig configs/defconfig``` for default.`
